const rp = require('request-promise');

var slackChannels = {
    'prod-alerts' : {
        webhook : 'https://hooks.slack.com/services/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'
    },
    'college-alerts' : {
        webhook : 'https://hooks.slack.com/services/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'
    },
    'course-alerts' : {
        webhook : 'https://hooks.slack.com/services/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'
    },
    'department-alerts' : {
        webhook : 'https://hooks.slack.com/services/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'
    },
    'doc-alerts' : {
        webhook : 'https://hooks.slack.com/services/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'
    },
    'search-alerts' : {
        webhook : 'https://hooks.slack.com/services/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'
    },
    'critical-alerts' : {
        webhook : 'https://hooks.slack.com/services/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'
    },
    'user-alerts' : {
        webhook : 'https://hooks.slack.com/services/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'
    },
    'placement-sheet-alerts' : {
        webhook : 'https://hooks.slack.com/services/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'
    }
}

function notifySlack(text, channel){

    var options = {
        method : 'POST',
        uri : slackChannels[channel].webhook,
        json : true,
        body : {
            "text" : text
        }
    }
    console.log(text);
    return rp(options);
}

module.exports = notifySlack;

/**
 * Webhook sample - https://hooks.slack.com/services/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
 * Curl sample - curl -X POST -H 'Content-type: application/json' --data '{"text":"Hello, World!"}' https://hooks.slack.com/services/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
 * Add webhook for new channel - https://api.slack.com/apps/AME0383PE/incoming-webhooks?success=1
 */