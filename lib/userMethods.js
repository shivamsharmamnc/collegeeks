var {authenticate} = require('./../middleware/authenticate');
var {File} =  require('./../models/fileModel');
var {User} = require('./../models/userModel');

function userDetail(id,detail){

    if(detail == 'views'){
      return new Promise((resolve, reject) => {
        File.find({author:id}, (err,doc)=>{
          if(err || doc==null)
            resolve(0);
          count = 0;
          for(i=0;i<doc.length;i++)
            count += doc[i].views.length
          resolve(count)
        }) 
      })
    }
  
    if(detail == 'files'){
      return new Promise((resolve, reject) => {
        File.find({author:id}, (err,doc)=>{
          if(err || doc==null)
            resolve(0);
          resolve(doc.length)
        }) 
      })
    }
  
    else {
      return new Promise((resolve, reject) => {
        User.findById(ObjectId(id), function (err, user) {
            if(err) {
                reject();
                return null;
            }
            if(user)
              resolve(user[detail]);
            else
              reject()
        })
      })
    }
  }

  module.exports({userDetail})