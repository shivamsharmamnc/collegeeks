var {View} = require('./../models/viewModel');
var {Experience} = require('./../models/experienceModel');
var {File} = require('./../models/fileModel');

async function getViewCount(pageId , pageType){
    updateViewCount(pageId , pageType)
    if(pageType == 'exp-read-main' ){
        return (await Experience.findOne({_id : pageId})).views ;
    }
    if(pageType == 'read-main' ){
        return (await File.findOne({_id : pageId})).views ;
    }  
}

async function updateViewCount(pageId, pageType){
    let count = await View.countDocuments({pageId : pageId, pageType : pageType});

    if(pageType == 'exp-read-main'){
        let page = await Experience.findOne({_id : pageId}).exec()
        count =  count ? parseInt(count*3.5512432 + page.shift) : 0
        if(parseInt(page.viewsUpdatedOn) + 30*60*1000 < new Date().getTime() ){
            Experience.findOneAndUpdate({_id : pageId},{viewsUpdatedOn : new Date().getTime(), views : count }, (err)=>{
                if(err){
                    console.log(err);
                }
            })
        }
    }
}

module.exports = getViewCount