var filesData = []
var fileDiv = document.getElementsByClassName('p6n-table-row-checkbox-enabled')
for(i=0; i<fileDiv.length; i++){ 
	filesData.push(
		{
			name : fileDiv[i].children[1].textContent.trim(),
			size : fileDiv[i].children[2].textContent.trim() 
		}
	)
}
function getSizeInKb(size){
  if(size.includes('KB'))
		return parseInt(size)
	else if(size.includes('MB'))
		return parseInt(size)*1024
	else{
	console.log('unknown size format ' + size)
	return 0;
 }
}
var pdfFilesData = []
var pdfFilesSize = []
for(i=0; i<filesData.length; i++){ 
	if(filesData[i].name.includes('.pdf')){
		pdfFilesData.push(filesData[i])
		pdfFilesSize.push(getSizeInKb(filesData[i].size))
	}
}
var labelArr = []
for(i=0; i<pdfFilesSize.length; i++){
	labelArr.push(i+1)
}
console.log(JSON.stringify(pdfFilesSize.sort(function(a, b){return a-b})))
console.log(JSON.stringify(labelArr))
