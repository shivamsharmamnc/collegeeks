const nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    host: 'smtp.zoho.in',
    port: 465,
    secure: true, 
    auth: {
        user: 'hello@collegeeks.com',
        pass: 'c0ll3g33ks2313'
    }
});

function sendMail(to, subject, text){

    text = '<body><div class="main-wrapper" style=" background: #f2e3ba; margin: 0; border: 3px solid #A33A47; border-radius: 5px; max-width:600px;"> <div class="main-container"> <div style="padding:5px;  padding-top:30px; padding-left:15px">' + text + '<br>In case of any queries feel free to contact us on hello.collegeek@gmail.com.</div><div class="footer" style=" background: #A33A47; color: #fff; padding: 5px; margin-top: 25px; "> <img src="https://collegeeks.com/images/logo-full-40.png" style="    float: left;    width: 36px;   margin-right : 10px;     border-radius: 10%;"> <div class="f1"><span class="team"><b>Collegeeks</b></span><span class="logo" style=" float: right; height: 40px; width: 40px; border-radius: 5px; "></span></div> <div class="kgpStu" style="color:rgba(255,255,255,.9); font-size:12px;"><i>Community of Learners !</i></div> </div> </div> </div></body><span style="color:#fff">collegeeks</span>'

    var mailOptions = {
        from: '"Collegeeks 👻" admin@collegeeks.com',
        to: to,
        subject: subject,
        html: text
    };
    
    transporter.sendMail(mailOptions, function(error, info){
        if (error) { 
            console.log('Error sending email using NodeMailer!');
            console.log(error)
        }
    });
}

module.exports = {sendMail};

