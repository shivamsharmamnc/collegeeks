const mongoose = require('mongoose');

var DepSchema = new mongoose.Schema({
  creator:{
    type: String
  },
  name: {
    type:String,
    required: true,
    trim: true,
    minlength:1,
    unique: true
  },
  shortName:{
    type: String,
    trim:true,
    unique: true
  },
  status:{
    type: String,   // active, trial
  },
  description: {
    type: String
  },
  pos: {
    type: Number
  },
  createdOn:{
    type: Number
  },
  sems : [String]
});

var Department = mongoose.model('Department', DepSchema);

module.exports = {Department}
