const mongoose = require('mongoose');

const commentsSchema = new mongoose.Schema({
    author : String,
    authorName : String,
    text : String,
    image : String,
    timestamp : String,
    threadId : String
},{
    collection : 'comments'
});

const comments = mongoose.model('comments', commentsSchema);

module.exports = comments;