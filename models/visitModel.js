const mongoose = require('mongoose');

var VisitSchema = new mongoose.Schema({
  author : String,
  authorName : String,
  timestamp : String,
  college : String,
  company : String,
  date : String
});

var Visit = mongoose.model('Visit', VisitSchema);

module.exports = {Visit}
