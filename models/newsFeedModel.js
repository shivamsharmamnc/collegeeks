const mongoose = require('mongoose');

var NewsSchema = new mongoose.Schema({
  author:  String,
  authorName : String,
  content : Object,
  college : String,
  timestamp : String,
  views : Number,
  viewsUpdatedOn : String,
  shift : Number,
  images : Array,
  ogUrl : String,
  ogImage : String,
  ogTitle : String
});

var News = mongoose.model('News', NewsSchema);

module.exports = {News}
