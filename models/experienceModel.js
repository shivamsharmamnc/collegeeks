const mongoose = require('mongoose');

var ExperienceSchema = new mongoose.Schema({
  author:  String,
  authorName : String,
  title : String,
  description : Array,
  college : String,
  company : String,
  category : String,
  timestamp : String,
  views : Number,
  viewsUpdatedOn : String,
  shift : Number
});

var Experience = mongoose.model('Experience', ExperienceSchema);

module.exports = {Experience}
