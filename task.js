var {College} = require('./models/collegeModel');

College.findOneAndUpdate({shortName: 'iit-guwahati'}, {$set:{address:"Surjyamukhi Road, North, Amingaon, Guwahati, Assam 781039", description:"Indian Institute of Technology Guwahati is a public institution established by the Government of India, located in Guwahati, in the state of Assam in India. It is the sixth Indian Institute of Technology established in India.", picture:"https://storage.googleapis.com/mlbu/jowevotu-maxresdefault.jpg"}}, {new: true}, (err, doc) => {
    if (err) {
        console.log("Something wrong when updating data!");
    }

    console.log(doc);
});
