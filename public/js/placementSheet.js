var editor;
$('document').ready(function (){
    $('.filterControls-title').click(function(){
        $('.filterControls-controller').slideToggle();
    });
    $('.addExperience-title').click(function(){
        $('.addExperience-panel').slideToggle();
    });
    $('.companyHistory-title').click(function(){
        $('.company-history').slideToggle();
    });

    $('.exp-post').click(function(){
        saveExperience();
    })
    $('.post-visit-btn').click(function(){
        saveVisit();
    });
    $('.loadMore').click(function(){
        loadMore('exp');
    })
    $('.apply-filter').click(function(){
        loadExperiences();
    })
    $('.removeFilter').click(function(){
        removeFilter();
    })
    setupCollegeAndCompanies();
    loadExperiences();
    loadVisits();
    setupEditor();
})

function loadColleges(){
    $.ajax({
        url : '/training/collegeList',
        method : 'POST',
        contentType : 'application/json',
        data : null,
    }).then(function(data){
        $('.exp-college').autocomplete({
            source : data
        })
    }).fail(function(err) {
        console.log(err);
    })
}

function loadCompanies(){
    $.ajax({
        url : '/training/companyList',
        method : 'POST',
        contentType : 'application/json',
        data : null,
    }).then(function(data){
        $('.exp-college').autocomplete({
            source : data
        })
    }).fail(function(err) {
        console.log(err);
    })
}

function loadVisits(){
    $.ajax({
        url : '/training/placement-sheet/listVisits',
        method : 'GET'
    }).then(function (data){
        setupVisits(data);
    }).fail(function(err){
        console.log(err);
    })
}

async function saveExperience(){

    try {
        var descriptionData = await editor.save()
    } catch (error) {
        console.log(error);
        descriptionData = null;
    }

    let experience = {
        title : $('.exp-title').val(),
        description : descriptionData,
        college : $('.exp-college').val(),
        company : $('.exp-company').val(),
        category : $('.exp-category').val()
    }
    $.ajax({
        url : '/training/placementSheet/addExperience',
        method : 'POST',
        contentType : 'application/json',
        data : JSON.stringify(experience)
    }).then(function(){
        toast('Green', 'Post created succesfully');
        loadExperiences();
        $('.addExperience-panel').slideToggle();
        $('.exp-college').val("");
        $('.exp-company').val("");
        $('.exp-description').html("");
        setupEditor();
    }).fail(function(err){
        console.log(err);
        let resp = err.responseText ? err.responseText : 'An error occured while creating your post!'
        toast('Red', resp)
    })
}

function loadExperiences(skip){
    $.ajax({
        url : '/training/placementSheet/listExperience',
        method : 'POST',
        contentType : 'application/json',
        data : JSON.stringify({
            college : $('.college-filter').val(),
            company : $('.company-filter').val(),
            category : $('.category-filter').val()
        }),
        headers : {
            skip : skip ? skip : 0
        }
    }).then(function(data){
        setupExperience(data, skip);
    }).fail(function(e){
        console.log(e);
    })
}

function setupExperience(expData, skip){
    $('.endOfResult').hide();
    $('.loadMore').hide();
    $('.notyet').hide();
    $('.spinner').hide();

    if($('.college-filter').val() != "0" || $('.company-filter').val() != "0" || $('.category-filter').val() != "0"){
        $('.filterActive').show();
    }else{
        $('.filterActive').hide();
    }

    let data = expData.data;
    let count = 50;
    let desc_max_size = 470;
    let loadedExp = $('.experience').length;
    if(loadedExp + expData.data.length < expData.length && skip){
        $('.loadMore').show()
    }else if(expData.data.length < expData.length && !skip){
        $('.loadMore').show()
    }
    else{
        $('.loadMore').hide();
        $('.endOfResult').show();
    }
    if(data.length == 0 && !skip){
        $('.endOfResult').hide()
        $('.experience-list-wrapper').html('')
        $('.notyet').show()
    }else{
        if(!skip) $('.experience-list-wrapper').html('')
        $('.notyet').hide();
        for(let i=0 ; i<data.length; i++){
            let exp = data[i];
            let shortTitle = exp.title.split(' ').join('-').toLowerCase();
            let shortDescription = prepareDescription(exp.description[0])
            if(shortDescription.length > desc_max_size)
                shortDescription = shortDescription.slice(0,desc_max_size) + ' ...'
            a = '<div class="experience">'
            b = '<div class="experience-title"><a target="_blank" href="/training/placement-sheet/experience/' + shortTitle + '?v=' + exp._id + '">' + exp.title + '</a></div>'
            c = '<div class="experience-description">' + shortDescription  +'</div>'
            d = '<div class="experience-details"><span class="exp-author-info">Posted ' + moment(parseInt(exp.timestamp)).fromNow() + ' by <a href="/user/' + exp.author + '">' + exp.authorName +'</a>.</span> <span class="exp-other-info">' + getCategory(exp.category) +'</span></div>'
            e = '</div>'
            $('.experience-list-wrapper').append(a+b+c+d+e);
        }
    }
}

function prepareDescription(data){
    let desc = "";
    for(let i=0; i<data.blocks.length; i++){
        let block = data.blocks[i];
        if(block.type == 'linkTool'){
            desc += " " + block.data.link + " ";
        }else if(block.type == 'header'){
            desc += " " + block.data.text + " "
        }else if(block.type == 'paragraph'){
            desc += " " + block.data.text + " "
        }else if(block.type == 'list'){
            desc += " " + block.data.items.join(' ') + " "
        }else if(block.type == 'code'){
            desc += " " + block.data.code.split('\n').join(' ') + " "
        }else if(block.type == 'table'){
            desc += " " + block.data.content.join(' ') + " "
        }else if(block.type == 'image'){
            desc += " -image- "
        }
    }
    desc = extractContent(desc);
    return desc.trim().split('  ').join(' ');
}

function saveVisit(){
    let visit = {
        college : $('.visit-college').val(),
        company : $('.visit-company').val(),
        date : $('.visit-date').val()
    }
    $.ajax({
        url : '/training/placementSheet/postVisit',
        method : 'POST',
        contentType : 'application/json',
        data : JSON.stringify(visit)
    }).then(function(){
        toast('Green', 'Post created succesfully');
        $('.visit-college').val("");
        $('.visit-company').val("");
        $('.visit-date').val("");
        loadVisits();
    }).fail(function(err){
        console.log(err);
        let resp = err.responseText ? err.responseText : 'An error occured while creating your post!'
        toast('Red', resp)
    })   
}

function setupVisits(data){
    let visit;
    $('.visit-list').html("");
    for(let i=0; i<data.length; i++){
        visit = data[i];
        a = '<div class="visit">'
        b = visit.company + ' @ ' + visit.college + ' on ' + visit.date 
        c = ' - <a href="/user/' + visit.author + '">' + visit.authorName + '</a></div>'
        $('.visit-list').append(a+b+c);
    }
}

function loadMore(type){
    if(type == 'exp'){
        count = 50;
        skip = $('.experience').length/count;
        loadExperiences(Math.ceil(skip))
    }
}

function setupCollegeAndCompanies(){
    $.ajax({
        url : '/training/getCollegeList',
        method : 'GET'
    }).then(function(data){
        $('#exp-college').autocomplete({
            source : data
        })
        $('#college-visit').autocomplete({
            source : data
        })
        for(let i=0; i<data.length; i++){
            $('.college-filter').append($("<option></option>")
                    .attr("value",data[i])
                    .text(data[i])); 
        }
    })
    $.ajax({
        url : '/training/getCompanyList',
        method : 'GET'
    }).then(function(data){
        $('#exp-company').autocomplete({
            source : data
        })
        $('#company-visit').autocomplete({
            source : data
        })
        for(let i=0; i<data.length; i++){
            $('.company-filter').append($("<option></option>")
                    .attr("value",data[i])
                    .text(data[i])); 
        }
    })
}

function removeFilter(){
    $('.college-filter').val("0");
    $('.company-filter').val("0");
    $('.category-filter').val("0");
    loadExperiences();
}

function getCategory(cat){
    if(cat == 'placement')
        return 'Placement'
    else if (cat == 'internship')
        return 'Internship'
}

function extractContent(s) {
    var span = document.createElement('span');
    span.innerHTML = s;
    return span.textContent || span.innerText;
  };

function setupEditor(){
    editor = new EditorJS({
        /**
         * Id of Element that should contain the Editor
         */
        holder : 'expDescription',
        placeholder: 'Please describe in detail -: Questions asked, Profile opened for, CPI/CGPA cut off, Examination platform, your current semester, Departments open for, Degrees open for.',
      
        /**
         * Available Tools list.
         * Pass Tool's class or Settings object for each Tool you want to use
         */
        tools: {
            header: {
                class: Header,
                inlineToolbar: ['link'],
                config: {
                  placeholder: 'Header'
                },
                shortcut: 'CMD+SHIFT+H'
              },
              /**
               * Or pass class directly without any configuration
               */
              image: {
                class: ImageTool,
                config: {
                  endpoints: {
                    byFile: '/upload/image', // Your backend file uploader endpoint
                    byUrl: '/fetchUrl', // Your endpoint that provides uploading by Url
                  }
                }
              },
              list: {
                class: List,
                inlineToolbar: true,
                shortcut: 'CMD+SHIFT+L'
              },
              checklist: {
                class: Checklist,
                inlineToolbar: true,
              },
              quote: {
                class: Quote,
                inlineToolbar: true,
                config: {
                  quotePlaceholder: 'Enter a quote',
                  captionPlaceholder: 'Quote\'s author',
                },
                shortcut: 'CMD+SHIFT+O'
              },
              warning: Warning,
              marker: {
                class:  Marker,
                shortcut: 'CMD+SHIFT+M'
              },
              code: {
                class:  CodeTool,
                shortcut: 'CMD+SHIFT+C'
              },
              delimiter: Delimiter,
              inlineCode: {
                class: InlineCode,
                shortcut: 'CMD+SHIFT+C'
              },
              linkTool: {
                  class : LinkTool,
                  config : {
                      endpoint : '/training/fetchUrl'
                  }
              },
              embed: Embed,
              table: {
                class: Table,
                inlineToolbar: true,
                shortcut: 'CMD+ALT+T'
              }
        },
      
        /**
         * Previously saved data that should be rendered
         */
        data: {}
      });
}