$('document').ready(function () {
      $('.upvote').click(function(){
        castVote('up')
      })

      $('.downvote').click(function(){
        castVote('down');
      })

      $('.shareDoc').click(function(){
        var text = window.location.href;
        navigator.clipboard.writeText(text).then(function() {
          toast('green', 'Sharable link copied !');
        }, function(err) {
          console.log(err);
        });
      })

      setupVotes();
      viewCount();
      fetchDescriptionData();
      reduceDescription();
})

function fetchDescriptionData(){
  $.ajax({
    url : '/training/placementSheet/getExperience/' + pageData.pageId,
    method : 'GET',
    body : null,
  }).then(function (data){
    setupExperienceDescription(data);
  }).fail(function(err){
    console.log(err);
  })
}

function reduceDescription(){
  if($('.description').html().length > 300 ){
    $('.description').html( extractContent($('.description').html()).slice(0,300) + ' ...' )
  }else{
    $('.description').html( extractContent($('.description').html()))
  }
}

function extractContent(s) {
  var span = document.createElement('span');
  span.innerHTML = s;
  return span.textContent || span.innerText;
};

function setupExperienceDescription(data){
  editor = new EditorJS({
    holder : 'exp-reader',
    tools: {
        header: {
            class: Header,
            inlineToolbar: ['link'],
            config: {
              placeholder: 'Header'
            },
            shortcut: 'CMD+SHIFT+H'
          },
          image: {
            class: SimpleImage,
            inlineToolbar: ['link'],
          },
          list: {
            class: List,
            inlineToolbar: true,
            shortcut: 'CMD+SHIFT+L'
          },
          checklist: {
            class: Checklist,
            inlineToolbar: true,
          },
          quote: {
            class: Quote,
            inlineToolbar: true,
            config: {
              quotePlaceholder: 'Enter a quote',
              captionPlaceholder: 'Quote\'s author',
            },
            shortcut: 'CMD+SHIFT+O'
          },
          warning: Warning,
          marker: {
            class:  Marker,
            shortcut: 'CMD+SHIFT+M'
          },
          code: {
            class:  CodeTool,
            shortcut: 'CMD+SHIFT+C'
          },
          delimiter: Delimiter,
          inlineCode: {
            class: InlineCode,
            shortcut: 'CMD+SHIFT+C'
          },
          linkTool: {
              class : LinkTool,
              config : {
                  endpoint : '/training/fetchUrl'
              }
          },
          embed: Embed,
          table: {
            class: Table,
            inlineToolbar: true,
            shortcut: 'CMD+ALT+T'
          }
    },
    data: data[0]
  });
}


function castVote(thumbs){
    voteData = {
        itemId : 'exp-read-' + pageData.pageId,
        voteType : thumbs
    }
    $.ajax({
        method : 'POST',
        url : '/vote/cast',
        contentType : 'application/json',
        data : JSON.stringify(voteData)
    }).done(function(){
        setupVotes();
    }).fail((err)=>{
        toast('red', 'Please signin to post your opinion.')
    })
}

function nFormatter(num, digits) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1E3, symbol: "k" },
      { value: 1E6, symbol: "M" },
      { value: 1E9, symbol: "G" },
      { value: 1E12, symbol: "T" },
      { value: 1E15, symbol: "P" },
      { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
}

function createCookie(name,value,minutes) {
    if (minutes) {
        var date = new Date();
        date.setTime(date.getTime()+(minutes*60*1000));
        var expires = "; expires="+date.toGMTString();
    } else {
        var expires = "";
    }
    document.cookie = name+"="+value+expires+"; path=/";
}

function viewCount(){
    $.ajax({
        method : 'POST',
        url : '/view',
        contentType : 'application/json',
        data : JSON.stringify(pageData)
    })
}

function setupVotes(){
    voteCountData = {
        itemId : 'exp-read-' + pageData.pageId
    }
    $.ajax({
        method : 'POST',
        url : '/vote/count',
        data : JSON.stringify(voteCountData),
        contentType : 'application/json'
    }).done(function(data){
        $('.like-count').text(nFormatter(data,1))
    })
}