var dep, sem, college, editor;
var currentState = window.history.state;

$('document').ready(function () {

    setDeps()
        .then(() => {
            return sortDeps();
        })
        .then(() => {
            return setClicks();
        })
        .then(() => {
            return presetDeps();
        });
    
    $('.clg-cnt-btn').click(function(){
        $('.clg-cnt').hide();
        cnt = $(this).attr('data-btn');
        $(cnt).fadeIn(300);
        if(cnt == '.news-cnt-wrapper'){
            loadNews();
        }
    })

    $('.addcoursebtn').click(() => {
        $('.add-course-wrapper').slideToggle('fast', function() {});
    });

    $('.adddepbtn').click(() => {
        $('.add-dep-wrapper').slideToggle( 'fast', function(){});
    });

    $('.news-post-btn').click(()=>{
        savePost();
    })

    $('.c-save').click(() => {
        saveCourse();
    });

    $('.d-save').click(() => {
        saveDepartment();
    });

    $('.document').on('click', '.upvote', function(){
        upvote(this);
    });

    loadNews();

    loadCollegeCourses();

    viewCount();
    
    window.onpopstate = function(event){
        setHistory(event);
    }

    setupEditor();
    
});

function loadNews(){
    $.ajax({
        method : 'GET',
        url : '/news/loadNews?college=' + college
    }).done((data)=>{
        $('.news-wrapper').html('');
        setupNews(data);
    }).fail((err)=>{
        console.log(err);
    })
}

function setupNews(data){
    if(data){
        for(let itr=0; itr<data.length; itr++){
            let news = data[itr];
            let id = news._id;
            a = '<div class="news-item"><div class="news-author-info"><span class="author-img" style="background-image:url('+ news.authorPic +')"></span>'
            b = '<span class="author-otherInfo"><span class="author-name"><a href="/user/'+ news.author +'">'+ news.authorName +'</a></span>'
            c = '<span class="post-time">'+ moment(parseInt(news.timestamp)).fromNow() +'</span></span></div>'
            d = '<div class="news-detail">'+ getNewsPostContent(news.content) + '</div>'
            e = '<div class="news-control"><div class="actions"><div class="like-doc" ><div class="action-btn upvote" data-item-id="' + id + '" onclick="upvoteItem(\''+ id +'\')"></div>';
            f = '<span class="like-count noselect" data-item-id="'+ id +'">'+ news.votes +'</span></div>'
            g = '<div class="comment-doc" ><div class="action-btn comment" data-item-id="'+ id +'" onclick="openThread(\''+ id +'\')"></div>';
            h = '<span class="comment-count noselect" data-item-id="'+ id +'">'+ news.comments +'</span></div>'
            i = '<div class="share" title="Share"><div class="action-btn shareDoc" data-item-id="'+ id +'"></div></div>'
            j = '</div><div class="comment-section" data-thread-id="'+ id +'"><div class="comment-list" data-thread-id="'+ id +'"></div>'
            k = '<div class="create-comment"><input type="text" class="create-comment" placeholder="Type here to create comment." data-thread-id="'+ id +'" onkeyup="keyUpInComment(\'' + id + '\')"></div></div></div>'

            $('.news-wrapper').append(a+b+c+d+e+f+g+h+j+k);

        }
    }
}

function getNewsPostContent(data){
    let content = '';
    if(data){
        for(let i=0; i<data.length; i++){
            let elem = data[i];
            
            if(elem.data){
                if(elem.type == 'header')
                content += '<div class="news-post-header">'+ elem.data.text +'</div>'
            
                if(elem.type == 'paragraph')
                    content += '<div class="news-post-para">'+ elem.data.text +'</div>'
                
                if(elem.type == 'image'){
                    if(elem.data.file){
                        content += '<a href="'+ elem.data.file.url +'" target="_blank"><div class="news-post-image" style="background-image:url(\''+ elem.data.file.url +'\')"></div></a>'
                        if(elem.data.caption){
                            content += '<div class="news-post-imageCaption">'+ elem.data.caption +'</div>'
                        }
                    }
                }
                
                if(elem.type == 'linkTool'){
                    let linkCard = ''
                    if(elem.data.meta){
                        if(elem.data.meta.title)
                            linkCard += '<div class="linkCard-title">'+ elem.data.meta.title +'</div>'
                        if(elem.data.meta.description)
                            linkCard += '<div class="linkCard-description">'+ elem.data.meta.description +'</div>'
                        if(elem.data.meta.domain)
                            linkCard += '<div class="linkCard-domain">'+ elem.data.meta.domain +'</div>'
                        content += '<a href="'+ elem.data.link +'" target="_blank"><div class="linkCard">'+ linkCard +'</div></a>'
                    }
                }
            }
                
        }
    }
    return content;
}

function keyUpInComment(threadId){
    let e = event;
    let code = e.keyCode || e.which;
    if(code == 13) {
        let commentObj = {
            text : $('.create-comment[data-thread-id="'+ threadId +'"]').val(),
            threadId : threadId
        }
        $.ajax({
            method : 'POST',
            url : '/comment/create',
            data : JSON.stringify(commentObj),
            contentType : 'application/json'
        }).done((msg)=>{
            loadComments(commentObj.threadId)
        }).fail((err)=>{
            toast('red', err.responseText)
        })
        $('.create-comment[data-thread-id="'+ threadId +'"]').val("")
    }
}


function setupCommentThread(threadId, data){
    if(data && data.length){
        $('.comment-list[data-thread-id="' + threadId + '"]').html('');
        for(let i=0; i<data.length; i++){
            comment = data[i];
            a = '<div class="comment-item"><span class="comment-author-name">';
            b = '<a href="/user/' + comment.author + '">' + comment.authorName + '</a></span>'
            c = '<span class="comment-text"> : ' + comment.text + '</span>';
            d = '<span class="comment-time"> - ' + moment(parseInt(comment.timestamp)).fromNow() + '</span></div>'

            $('.comment-list[data-thread-id="' + threadId + '"]').append(a+b+c+d);
        }
        $('.comment-count[data-item-id="'+ threadId +'"]').text(data.length);
    }
}

function loadComments(threadId){
    $.ajax({
        method : 'GET',
        url : '/comment/getThreadComments/' + threadId
    }).done((data)=>{
        setupCommentThread(threadId, data);
    }).fail((err)=>{
        console.log(err.responseText);
    })
}


function upvote(elem){
    let voteData = {
        itemId : 'news-' + $(elem).attr('data-item-id'),
        voteType : 'up'
    }

    $.ajax({
        method : 'POST',
        url : '/vote/cast',
        contentType : 'application/json',
        data : JSON.stringify(voteData)
    }).done(()=>{
        setupVote(voteData.itemId);
    }).fail((msg)=>{
        toast('red', msg.responseText);
    })
}

function upvoteItem(id){
    let voteData = {
        itemId : 'news-' + id,
        voteType : 'up'
    }

    $.ajax({
        method : 'POST',
        url : '/vote/cast',
        contentType : 'application/json',
        data : JSON.stringify(voteData)
    }).done(()=>{
        setupVote(voteData.itemId);
    }).fail((msg)=>{
        toast('red', msg.responseText);
    })
}

function openThread(id){
    $('.comment-section[data-thread-id="'+ id +'"]').slideToggle();
    loadComments(id);
}

function setupVote(id){
    $.ajax({
        method : 'POST',
        url : '/vote/count',
        contentType : 'application/json',
        data : JSON.stringify({
            itemId : id
        })
    }).done((data)=>{
        id = id.split('-')[1];
        $('.like-count[data-item-id="'+ id +'"]').text(data)
    })
}

function setHistory(event){
    var eventState = event.state;
        if(currentState == null){
            currentState = {
                page : 'depList',
                level : 0
            }
        }
        if(eventState == null){
            eventState = {
                page : 'depList',
                level : 0
            }
        }
        if(eventState.level < currentState.level){
            currentState = eventState
            backClick();
        }else{
            if(currentState.level == 1){
                window.history.replaceState({page:'depList', level:0}, this.undefined, window.location.pathname)
                backClick();
            }else{
                backClick();
            }
        }
}

function saveDepartment(){
    formData = {
        name : $('.d-name').val(),
        college: college
    }

    $.ajax({
        url: "/college/adddep",
        method: "POST",
        contentType : 'application/json',
        data: JSON.stringify(formData),
      }).done(function( msg ) {
        toast('green','Application Submitted!');
        $('.d-name').val('');
        $('.adddepbtn').click();
      }).fail(function( jqXHR, status, error ) {
        console.log( "AddDep request failed: " + jqXHR.status );
        if(jqXHR.status == 401)
            toast('red','Please sign-in to complete action');
        if(jqXHR.status == 500)
            toast('red','An Internal error occured. Please try again!');
      });
}

function saveCourse(){
    formData = {
        name : $('.c-name').val(),
        college: college,
        department: dep,
        semester: sem
    }

    fdata = {
        data : JSON.stringify(formData)
    }

    $.ajax({
        url: "/course/addcourse",
        method: "POST",
        data: fdata,
      }).done(function( msg ) {
        toast('green', msg);
        $('.c-name').val('');
        $('.add-course-wrapper').slideToggle('fast', function() {});
        loadCourses();
      }).fail(function( jqXHR, status, error ) {
        console.log( "AddCollege request failed: " + jqXHR.status );
        if(jqXHR.status == 401)
            toast('red','Please sign-in to complete action');
        if(jqXHR.status == 400)
            toast('red','College with this name already exist!');
        if(jqXHR.status == 500)
            toast('red','An Internal error occured. Please try again!');
      });
}

function setDeps(){
    return new Promise((resolve, reject) => {
        college = window.location.href.split('?')[0].split('/')[4];
        $.ajax({
            url : '/college/collegeDepList/' + college,
            method : 'GET'
        }).then(function(data){
            if(data.length == 0)
                toast('black', 'No departments added')
            for(i=0; i<deps.length; i++){
                if(data.includes(deps[i].shortName)){
                    a = '<div class="dep item" dep="' + deps[i].shortName + '" style="background-position-y:';
                    b = '-' + deps[i].pos*32 + 'px">'
                    c = deps[i].name
                    d = '</div>'
                    $('.selectbox').append(a+b+c+d);
                }
            }
            resolve()
        }).fail(function(err){
            console.log(err);
        })
    });
}

function setupCourse(msg){
    return new Promise((resolve, reject) => {
        $('.spinner').hide();
        $('.notyet').hide();
        $('.c-list').html('');
        if(msg.length == 0) $('.notyet').show();
        for(i=0; i<msg.length; i++){
            a = '<a href="/course/' + msg[i].shortName + '?college=' + msg[i].college + '&v=' + msg[i]._id + '" > <div class="course item">';
            b = msg[i].name;
            c = '</div></a>'
            $('.c-list').append(a+b+c);
        }
    })
}

function loadCourses(){
    $('.spinner').show();
    $('.notyet').hide()
    return new Promise((resolve, reject) => {
        formData = {
            college: college,
            department: dep,
            semester: sem
        }

        fdata = {
            data : JSON.stringify(formData)
        }

        $.ajax({
            url: "/course/list",
            method: "POST",
            data: fdata,
          }).done(function( data ) {
            setupCourse(data);
          }).fail(function( jqXHR, status, error ) {
            setupCourse([]);
            console.log( "GET Course list request failed: " + jqXHR.status );
          });
    })
}

function sortDeps(){
    return new Promise((resolve, reject) => {
        $('.dep').sort(function (a,b) {
            if (a.textContent < b.textContent) {
                return -1;
            } else {
                return 1;
            }
        }).appendTo('.selectbox');
        resolve();
    });
}

function presetDeps(){
    return new Promise((resolve, reject) => {
        var currentUrl = new URL(window.location.href)
        var dep = currentUrl.searchParams.get('department')?currentUrl.searchParams.get('department'):$.cookie('dep');
        var depel = document.querySelector('[dep="'+dep+'"]');
    
        if(depel)
        depel.click();

        var sem = currentUrl.searchParams.get('sem')?currentUrl.searchParams.get('sem'):$.cookie('sem');
        var semel = document.querySelector('[semId="'+sem+'"]');
       
        if(semel)
            semel.click();
        
        resolve();
    });
}

function setupSemesters(department){
    document.getElementById('semList').innerHTML = '';
    deps.forEach(function(dep){
        if(dep.shortName==department){
            var semesters = dep.sems;
            semesters.forEach(function(sem){
                semId = sem.toLowerCase().split(' ').join('-');
                a = '<div class="sem item" onclick="semClick(\'' + sem + '\',\'' + semId + '\')" semId="' + semId + '" >';
                b = sem;
                c = '</div>'
                document.getElementById('semList').innerHTML += (a+b+c)
            })
        }
    })
}

function semClick(semName, semId) {
    $('.c-list').html('');
    $('.seperator').removeClass('hidden');
    $('.current-sem').text(semName) 
    sem = semId;
    $.cookie('sem', sem, { expires: 300 });
    $('.semesters').addClass('hidden');
    $('.courses').removeClass('hidden');
    loadCourses();
    if(location.search)
        var newParam = location.search + '&sem=' + sem;
    else
        var newParam = '?sem=' + sem;
    window.history.pushState({
        page : 'courseList',
        level : 2
    }, undefined, newParam);
    currentState = history.state;
}

function backClick(){
    if($('.current-sem').text() != ''){
        $.removeCookie('sem');
        $('.seperator').addClass('hidden');
        $('.current-sem').text('');
        $('.semesters').removeClass('hidden');
        $('.courses').addClass('hidden');
    } else if ($('.current-dep').text() != ''){
        $.removeCookie('dep');
        $('.current-dep').text('');
        $('.departments').removeClass('hidden');
        $('.semesters').addClass('hidden');
    }
}

function setClicks(){
    return new Promise((resolve, reject) => {    
        $('.dep').click(function() {
            $('.current-dep').text($(this).text())
            dep = $(this).attr('dep')
            $.cookie('dep', dep, { expires: 300 });
            $('.departments').addClass('hidden');
            setupSemesters(dep);
            $('.semesters').removeClass('hidden');
            window.history.pushState({
                page : 'semList',
                level : 1
            }, undefined, '?department=' + dep);
            currentState = history.state;
        });

        $('.sem').click();
    
        $('.back').click(function () {
            window.history.back();
        });

        resolve();
    })
}

function loadCollegeCourses(){
    $.ajax({
        url : '/college/courseList',
        method : 'POST',
        contentType : 'application/json',
        data : JSON.stringify({college}),
    }).then(function(data){
        $('#courseName').autocomplete({
            source : data
        })
    }).fail(function(err) {
        console.log(err);
    })
}

function viewCount(){
    var data = {
        pageId : college,
        pageType : 'college-main',
        college : college
    }

    $.ajax({
        method : 'POST',
        url : '/view',
        contentType : 'application/json',
        data : JSON.stringify(data)
    })
}


async function savePost(){

    try {
        var descriptionData = await editor.save()
    } catch (error) {
        console.log(error);
        descriptionData = null;
    }

    let content = descriptionData.blocks
    
    let newsObj = {
        content : content,
        college : college
    }

    $.ajax({
        method : 'POST',
        url : '/news/save',
        contentType : 'application/json',
        data : JSON.stringify(newsObj)
    }).done((msg)=>{
        $('.news-wrapper').html('');
        loadNews();
        $('#newsDescription').html("");
        setupEditor();
    }).fail((msg)=>{
        toast('red', msg.responseText);
    })
}


function setupEditor(){
    editor = new EditorJS({
        /**
         * Id of Element that should contain the Editor
         */
        holder : 'newsDescription',
        minHeight : 20,
        placeholder: 'Start your post.!',
      
        /**
         * Available Tools list.
         * Pass Tool's class or Settings object for each Tool you want to use
         */
        tools: {
            header: {
                class: Header,
                inlineToolbar: ['link'],
                config: {
                  placeholder: 'Header'
                },
                shortcut: 'CMD+SHIFT+H'
              },
              /**
               * Or pass class directly without any configuration
               */
              image: {
                class: ImageTool,
                config: {
                  endpoints: {
                    byFile: '/upload/image', // Your backend file uploader endpoint
                    byUrl: '/fetchUrl', // Your endpoint that provides uploading by Url
                  }
                }
              },
              linkTool: {
                  class : LinkTool,
                  config : {
                      endpoint : '/training/fetchUrl'
                  }
              }
        },
      
        /**
         * Previously saved data that should be rendered
         */
        data: {}
      });
}