function populateCollege(data){
    for(i=0; i<data.length; i++){
        a1 = '<a href="/college/' + data[i].shortName + '">'
        a = '<div class="gm-item"><div class="gm-item-title">'
        b = data[i].name;
        c = '</div><div class="gm-item-detail">'
        d = data[i].address
        e = '</div><div class="gm-item-count">'
        f = nFormatter(data[i].docCount, 1) + ' Documents ' + nFormatter(data[i].courseCount, 1) + ' Courses'
        g = '</div></div></a>'
        $('.gm-body').append(a1+a+b+c+d+e+f+g)
    }
}

function nFormatter(num, digits) {
  var si = [
    { value: 1, symbol: "" },
    { value: 1E3, symbol: "k" },
    { value: 1E6, symbol: "M" },
    { value: 1E9, symbol: "G" },
    { value: 1E12, symbol: "T" },
    { value: 1E15, symbol: "P" },
    { value: 1E18, symbol: "E" }
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
}

$('document').ready(()=> {
    $.ajax({
        url: "/college/collegelist",
        method: "POST"
      }).done(function( msg ) {
        populateCollege(msg);
      }).fail(function( jqXHR, status, error ) {
        console.log('Error loading colleges')
      });
});