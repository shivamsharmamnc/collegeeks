
var device = (window.innerWidth > 480) ? "desktop" : "mobile";
window.onresize = function(e) { device = (window.innerWidth > 480) ? "desktop" : "mobile";}

$(document).ready(function() {
    // switch between login and signup
    $('.signup-tab').click(function(){
        $('.login-form').hide();
        $('.login-tab').removeClass('active');
        $('.signup-tab').addClass('active');
        $('.signup-form').show();
    });
    $('.login-tab').click(function(){
        $('.signup-form').hide();
        $('.login-tab').addClass('active');
        $('.signup-tab').removeClass('active');
        $('.login-form').show();
    });
    
    $('#signupform').submit(function(event){
        $('.spinner').show();
        $('.email-taken').hide();
        event.preventDefault();
        input = document.querySelectorAll('.si');
        formData = {
            "name":input[0].value,
            "email":input[1].value
        }
        $.ajax({
            url: "/user/signup",
            method: "POST",
            data: formData
          }).done(function( msg ) {
            $('.spinner').hide();
            $('.login-tab').click();
            message = 'Hi <b>' + input[0].value.split(' ')[0] + '</b>, Welcome to Magguland. <br>Please Login using password sent to <i>' + input[1].value + '</i>.'
            $('.set').html(message);
            $('.set').show();
            input[3].value = input[1].value;
            input[4].focus();
          }).fail(function( jqXHR, status, error ) {
            console.log( "SignUp request failed: " + jqXHR.status );
            if(jqXHR.status == 400)
                $('.email-taken').show();
          });
    });

    $('#loginform').submit(function(event){
        $('.spinner').show();
        $('.wrong').hide();
        event.preventDefault();
        input = document.querySelectorAll('.si');
        formData = {
            "email":input[3].value,
            "password":input[4].value
        }
        $.ajax({
            url: "/user/login",
            method: "POST",
            data: formData
          }).done(function( msg, status, req ) {
            $('.spinner').hide();
            localStorage.setItem('x-auth',req.getResponseHeader('x-auth'))
            continueUrl = new URL(window.location.href).searchParams.get('continue');
            if(continueUrl)
                window.location = continueUrl;
            else
                window.location = '/';
          }).fail(function( jqXHR, status, error ) {
            console.log( "Login request failed: " + jqXHR.status );
            if(jqXHR.status == 400)
                $('.wrong').show();
          });
    });

});


