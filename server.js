var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var createError = require('http-errors');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var fs = require('fs');
var compression = require('compression')
// const http = require('http');
var https = require('https');
var {sendMail} = require('./lib/mailer');

// const socketio = require('socket.io');
// const minify = require('express-minify');
// const compression = require('compression');

var mlconfig = require('./../ml-config.json');  

var mainRoutes = require('./routes/mainApi');
var headerRoutes = require('./routes/header');
var userRoutes = require('./routes/userApi');
var collegeRoutes = require('./routes/collegeApi');
var readRoutes = require('./routes/readApi');
var gmRoutes = require('./routes/goldmineApi');
var simpleRoutes = require('./routes/simpleRoutes');
var uploadRoutes = require('./routes/upload');
//uploadRoutes = require('./routes/uploadApi');
var courseRoutes = require('./routes/courseApi');
var fileRoutes = require('./routes/fileApi');
var taskRoutes = require('./routes/taskApi');
var adminRoutes = require('./routes/adminApi');
var viewRoutes = require('./routes/viewApi');
var voteRoutes = require('./routes/voteApi');
var trainingRoutes = require('./routes/trainingApi');
var someTask = require('./routes/taskApi');
var newsRoutes = require('./routes/newsApi');
var commentRoutes = require('./routes/commentApi');
var covidRoutes = require('./routes/covidApi');

var notifySlack = require('./lib/slackNotifier');

var {mongoose} = require('./db/mongoose');

var app = express();


if(mlconfig.env == 'prod')
  var port = process.env.PORT || 80;
else
  var port = 3000;

if(mlconfig.env == 'prod'){
  var options = {
    key:fs.readFileSync('./ssl/private.key'),
    cert:fs.readFileSync('./ssl/certificate.crt'),  
    ca: [
      fs.readFileSync('./ssl/ca_bundle.crt'),
    ],
    requestCert: false,
    rejectUnauthorized: false
  };

  https.createServer(options, app).listen(443);

  app.use(function(req, res, next) {
    if(!req.secure) {
      return res.redirect(['https://', req.get('Host'), req.url].join(''));
    }
    if(req.get('Host') && req.get('Host').includes('magguland')){
      return res.redirect(['https://www.collegeeks.com', req.url].join(''));
    }
    if(req.get('Host') && !req.get('Host').includes('www')){
      return res.redirect(['https://www.collegeeks.com', req.url].join(''));
    }
    next();
  });
  //var io = socketio(server)
}


if (mlconfig.env == 'local') {
  // var server = http.createServer(app)
  // var io = socketio(server)
  app.use( (req, res, next) => {
    var date = new Date();
    process.stdout.write(date.toLocaleTimeString()+' ');
    next()
  });
  app.use(logger('dev'));
}

//app.use(compression({ filter: shouldCompress, threshold: 0 }));
app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(compression());
// app.use(minify());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());

app.use('/task', taskRoutes);
app.use('/user',userRoutes);
app.use('/header',headerRoutes);
app.use('/read', readRoutes);
app.use('/goldmine', gmRoutes);
app.use('/sr', simpleRoutes);
app.use('/college', collegeRoutes);
app.use('/upload', uploadRoutes);
app.use('/course', courseRoutes);
app.use('/file', fileRoutes);
app.use('/task', taskRoutes);
app.use('/admin', adminRoutes);
app.use('/view', viewRoutes);
app.use('/vote', voteRoutes);
app.use('/training', trainingRoutes);
app.use('/news', newsRoutes);
app.use('/comment', commentRoutes);
app.use('/covid', covidRoutes);
app.use('/', mainRoutes);

app.listen(port, () => {
  console.log(`Started up at port ${port}`);
});

// io.on('connection', (socket)=>{
//   var stay = {}
//   socket.on('join', (msg)=>{
//     stay.start = new Date().getTime();
//     stay.url = msg;
//   })
//   socket.on('pageType', (msg)=>{
//     stay.pageType = msg;
//   })
//   socket.on('pageId', (msg) =>{
//     stay.pageId = msg;
//   })
//   socket.on('disconnect', ()=>{
//     stay.end = new Date().getTime();

//     // save to view & hits collection
//   })
// })

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  if(err){
    var fileName = 'server.js';
    var apiName = req.method + ' ' + req.originalUrl
    if(typeof err == 'object'){
      var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
    }else{
      var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
    }
    if(err.name == 'NotFoundError')
      var slackText = '*404 ' + req.method + ' ' + req.originalUrl + '*';
    notifySlack(slackText, 'prod-alerts');
  }
  console.log('404 ' + req.method + ' ' + req.originalUrl);
  if(req.method == 'GET')
    res.render('error');
  else
    res.status(500).send('Error proccessing request. Status 500');
});

function shouldCompress (req, res) {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false
  }
 
  // fallback to standard filter function
  return compression.filter(req, res)
}

module.exports = {app};
