var express = require('express');
const _ = require('lodash');

const fileName = __filename;

const adminList = require('./../json/adminList.json');
var {authenticate} = require('./../middleware/authenticate');
var {File} = require('./../models/fileModel');
var {College} = require('./../models/collegeModel');
var {Course} = require('./../models/courseModel');
var {View} = require('./../models/viewModel');
var {Department} = require('./../models/departmentModel');
var {sendMail} = require('./../lib/mailer.js');
var notifySlack = require('../lib/slackNotifier');

var router = express.Router();
module.exports = router;

router.get('/addcollege' ,authenticate , (req, res) => {
    try {
    if(req.user==null){
        if(req.cookies.li != 1){
            res.redirect('/user/signin?continue=/college/addcollege')
        }
    }else{
        res.render('addcollege');
    }
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
});

router.post('/addcollege', authenticate, async (req, res) => {
    try {
    if(req.user==null)
        res.status(401).send('authentication failed');
    else{
        var body = JSON.parse(req.body.data);
        body.creator = req.user.id;
        body.createdOn = new Date().getTime();
        body.departments = body.departments ? body.departments : ["electrical", "geology", "aerospace", "ocean-engineering", "industrial", "mining", "law", "manufacturing", "architecture", "chemical", "chemistry", "civil-engineering", "computer-science", "ece", "mathematics", "humanities", "bio-technology", "agriculture", "mechanical", "physics", "entrepreneurship", "finance", "rubber", "placement", "firstyear"];
        body.status = 'trial';
        body.shortName = (body.name).toLowerCase().replace(/[^a-z0-9\ ]/gi,'').split(' ').join('-');
        var college = new College(body);
        college.save().then(()=>{
            text = 'Hi <b>' + req.user.name + '</b>,<br><br>We have have recieved your application to add your college named <b>' + body.name + '</b>. ';
            text += 'The college you added will be on trial i.e. Only you will have access to add/edit data. After 100+ genuine documents are uploaded your college will be made public. '
            text += '<br><br>We have added the default departments, if you want add more, delete some, or edit names please reply to same thread. '
            text += '<br><br>Once again thankyou for showing interest. For any query please feel free to reply.';
            subject = 'Congratulations! We have received your application to add ' + body.name;
            sendMail(req.user.email, subject, text);
            sendMail('shivamsharma.btp@gmail.com', 'Add new college request recieved', 'College : ' + body.name + ' Department not in list - ' + body.notInListDepartments);
            sendMail('rittewari92@gmail.com', 'Add new college request recieved', 'College : ' + body.name);
            notifySlack('Add my college request\n*College : ' + body.name + '*', 'college-alerts');
            res.status(200).send('sucess');
        }).catch((e)=>{
            notifySlack('Add my college request\n*College : ' + body.name + '* But failed to add college ```' + e + '```', 'college-alerts')
            sendMail('shivamsharma.btp@gmail.com', 'Add new college request failed ' ,'Add my college request\n*College : ' + body.name + '* But failed to add college ```' + e + '```')
            res.status(400).send(e);
        });
    }
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
});

router.post('/adddep', authenticate, (req, res) => {
    try {
    if(req.user == null){
        res.status(401).send('User not found!');
    }else{
        res.status(200).send('success');

        subject = 'Add ' + req.body.name + ' department in ' + req.body.college;
        subject2 = 'Congratulations! We have received your application to add ' + req.body.name + ' department in ' + req.body.college;
        requestCreatorText = 'Hi <b>' + req.user.name + '</b>,<br><br>We have recieved your application to add <b>' + req.body.name + '</b> department in ' + req.body.college + '.<br><br>We will process it within as soon as possible.In case of any query feel free to reply to same thread. <br><br>Thank-You for your contributions.';
        adminText = 'Add new college request recieved. Request created by: ' + req.user.name + ' , id : ' + req.user._id + ', college : ' + req.body.college + ', department : ' + req.body.name;
        sendMail(req.user.email, subject2, requestCreatorText );
        sendMail('shivamsharma.btp@gmail.com', subject, adminText);
        sendMail('rittewari92@gmail.com', subject, adminText);

        notifySlack('Add *' + req.body.name + '* department in *' + req.body.college + '*', 'department-alerts');
    }
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
})

router.get('/collegeDepList/:college', (req, res)=>{
    try {
    College.findOne({shortName:req.params.college}, (err, doc) => {
        if(err || doc == null){
            console.log(err);
            res.send(null);
            return;
        }else{
            res.json(doc.departments);
        }
    })
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
});

router.post('/courseList', (req, res) => {
    try {
    Course.find({college : req.body.college}, (err, docs)=>{
        if(err){
            console.log(err)
        }else{
            var courseNames = [];
            docs.forEach(doc => {
                courseNames.push(doc.name)
            })
            res.json(courseNames);
        }
    })
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
})

router.get('/:college', authenticate, async (req, res) => {
    try {
    var li = req.cookies.li;
    let courses = await Course.find({college : req.params.college}).exec();
    let courseData = [];
    for(let i=0; i<courses.length; i++){
        courseData.push({
            name : courses[i].name,
            shortName : courses[i].shortName,
            _id : courses[i]._id,
            college : req.params.college
        })
    }
    College.findOne({shortName:req.params.college},(err, doc) => {
        if(err || doc==null){
            console.log(err);
            res.render('error');
            return;
        }
        else{
            if(doc.status != 'trial' || true){
                res.render('college', {
                    li : li,
                    title: doc.name,
                    address: doc.address,
                    description: doc.description,
                    picture: doc.picture,
                    imgUrl : getDepOg(req.query.department) ? getDepOg(req.query.department) : doc.picture,
                    courseData : courseData
                });
            }else{
                if(req.user){
                    if(adminList.includes(req.user.email) || doc.creator == req.user.id){
                        res.render('college', {
                            li : li,
                            title: doc.name,
                            address: doc.address,
                            description: doc.description,
                            picture: doc.picture,
                            courseData : []
                        });
                    }else
                    res.render('error');
                }else
                res.render('error');
            }
        }
    })
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }

});

router.post('/collegelist', authenticate, (req,res) => {
    try {
    College.find( async (err, doc) => {
        if(err || doc==null)
            res.status(500).send(err);
        collegeData = [];
        for(i=0;i<doc.length;i++){
            var cData = {
                name: doc[i].name,
                shortName : doc[i].shortName,
                address : doc[i].address,
                courseCount : await Course.countDocuments({college : doc[i].shortName }),
                docCount : await File.countDocuments({college : doc[i].shortName}),
                //description : doc[i].description,
                //picture : doc[i].picture,
                //hits : await View.countDocuments({college : doc[i].shortName})
            }
            if(doc[i].status != 'trial'){
                collegeData.push(cData);
            }else{
                if(req.user){
                    if(adminList.includes(req.user.email) || doc[i].creator == req.user.id){
                        collegeData.push(cData);
                    }
                }
            }
        }
        res.json(collegeData);
    })
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
})

function getDepOg(department){
    if(department == 'computer-science')
        return 'https://www.collegeeks.com/images/ogs/computer-science-og.jpg';
    else if(department == 'geology')
        return 'https://www.collegeeks.com/images/ogs/geology-og.jpg';
    else return null;
}