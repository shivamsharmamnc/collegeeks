var express = require('express');
const _ = require('lodash');
const fileName = __filename;

var {authenticate} = require('./../middleware/authenticate');
var {Course} = require('./../models/courseModel');
var {File} = require('./../models/fileModel');
var notifySlack = require('../lib/slackNotifier');
var deps = require('../json/departments.json');

var router = express.Router();
module.exports = router;

router.post('/addcourse', authenticate, async (req, res) => {
    try {
    if(req.user==null)
        res.status(401).send('Please sign in to add course.');
    else{
        var body = JSON.parse(req.body.data);
        body.creator = req.user.id;
        body.createdOn = new Date().getTime();
        body.shortName = (body.name).toLowerCase().split('-').join(' ').replace(/\s+/g,' ').trim().replace(/[^a-z0-9\ ]/gi,'').split(' ').join('-');
        path = {
            college : body.college,
            department : body.department,
            semester : body.semester,
            pathId : body.college + '&' + body.semester + '&' + body.department
        }
        
        Course.findOne( {$or : [ {college : body.college, shortName : body.shortName},{college : body.college, name : body.name}]}, (err, doc)=>{
            if(err){
                console.log(err);
            }else{
                if(doc == null){
                    body.paths = [path]
                    var course = new Course(body);
                    course.save().then(()=>{
                        res.status(200).send('Course created and saved!');
                        notifySlack('New Course Added\n *Course Name : '+ body.shortName +'*\n*College : '+ body.college + '*\n*Department : ' + body.department + '*\n*Semester : ' + body.semester +  '*', 'course-alerts');
                    }).catch((e)=>{
                        res.status(400).send(e);
                    })
                }else{
                    if(!doc.paths.includes(path)){
                        Course.findOneAndUpdate({shortName: body.shortName, college : body.college}, {$push: {paths:path}}, (error, result)=>{
                            if(error)
                                console.log(error)
                            else{
                                res.status(200).send('Course saved!');
                            }
                        });
                    }else{
                        res.status(200).send('Course already saved here!')
                    }
                }
            }
        })

        
    }
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
})

router.post('/list', (req, res) => {
    try {
    var body = JSON.parse(req.body.data);
    var pathId = body.college + '&' + body.semester + '&' + body.department;

    Course.find({paths : {$elemMatch : {pathId : pathId}} }, (err, result) => {
        if(err || result==null)
            res.status(400).send('Courses not found!');
        res.status(200).send(result);
    });
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
})

router.get('/:cName' , async (req, res) => {
    try {
    let courseDocs = await File.find({courseIds : req.query.v}).exec();
    let docsData = [];
    for(let i=0; i<courseDocs.length; i++){
        docsData.push({
            name : courseDocs[i].name,
            v : courseDocs[i].uniqueId,
            shortName : courseDocs[i].name.toLowerCase().split(' ').join('-')
        })
    }
    Course.findOne({_id : req.query.v},(err, doc) => {
        //console.log(doc)
        if(err || doc==null){
            res.render('error');
        }
        else{
            res.render('course', {
                name: doc.name,
                pageId : doc._id,
                collegeId : doc.college,
                title: doc.name,
                description : doc.department + ' ' + doc.college,
                imgUrl : getImageUrl(doc.department),
                docsData : docsData
            });
        }
    });
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
})

function getImageUrl(dep){
    for(let i=0; i<deps.length; i++){
        if(deps[i].shortName == dep){
            if(deps[i].pos < 10) pos = '0' + deps[i].pos; 
            else pos = deps[i].pos;
            return "https://www.collegeeks.com/images/icons/deps/" + pos + '-' + dep + '.png'; 
        }
    }
    return null
}