const express = require('express');
const _ = require('lodash');
const uniqid = require('uniqid');
const nodemailer = require('nodemailer');
const generatePassword = require('password-generator');
const md5 = require('md5');
const cookieParser = require('cookie-parser');
const {OAuth2Client} = require('google-auth-library');
var ObjectId = require('mongodb').ObjectID;
var mlconfig = require('./../../ml-config.json');

const fileName = __filename;
const client = new OAuth2Client('66303115487-0l4aecii49r6at5icr799bqghd36k6bv.apps.googleusercontent.com');

var {authenticate} = require('./../middleware/authenticate');
var {File} =  require('./../models/fileModel');
var {User} = require('./../models/userModel');
var {View} = require('./../models/viewModel');
var {Course} = require('./../models/courseModel');

var router = express.Router();
module.exports = router;

var notifySlack = require('../lib/slackNotifier');
var {sendMail} = require('../lib/mailer');

if(mlconfig.env == 'local')
  g_token = "312975383578-aoia4cu34demtrstbhdkhg6shhjbiedc.apps.googleusercontent.com";
else if(mlconfig.env == 'prod')
  g_token = "66303115487-9khj5u66bs0vukej6skuc7ih5egltsa6.apps.googleusercontent.com"

// GET signin page
router.get('/signin', (req, res) => {
  res.render('signin',{
    g_token : g_token
  });
});

router.post('/google/signin', (req, res) => {
  try {
    date = new Date();
  async function verify() {
    const ticket = await client.verifyIdToken({
        idToken: req.body.idToken,
        audience: g_token
    });
    const payload = ticket.getPayload();
    if(payload.email_verified){
      var body = {
        'email' : payload.email,
        'name' : payload.name,
        'password' : payload.sub,
        'profilePicture' : payload.picture,
        'googlePhoto' : payload.picture,
        'timeLag' : date.getTime() - req.body.time,
        'joinDate' : date.getTime()
      }
      var user = new User(body);
      user.save().then(() => {
        res.status(200);
        user.generateAuthToken().then((token) => {
          res.cookie('li',1, { maxAge: 3075840000, path:'/' });
          res.cookie('x-auth', token, { maxAge : 3075840000, path : '/'});
          res.json({msg:'success'});
          let subject = 'Welcome to Collegeeks!';
          let text = 'Thank you for signing up <b>' + body.name.split(' ')[0] + '</b>.<br><br>We built Collegeeks to make college students (<i>as well as students who cant afford college</i>) to get an platform to study, discuss and share study material. We have put up a lot of effort to put up information in as much systematic way possible.<br><br>Cheers,<br>Shivam.'
          sendMail(body.email, subject, text);
          notifySlack('New User Registered - ' + body.name , 'user-alerts');
        });
      }).catch((e) => {
          User.findByCredentials(body.email, body.password).then((user) => {
            user.generateAuthToken().then((token) => {
              res.cookie('li',1, { maxAge: 30758400000, path:'/' });
              res.cookie('x-auth', token, { maxAge : 3075840000, path : '/'});
              res.json({msg:'success'});
            }).catch((e) => {console.log('error generating auth token')});
          }).catch((e) => {
            console.log('login failed')
            res.status(400).send(e);
          });
      })
    }else {
      throw new Error('token verification failed');
    }
  }
  verify().catch( (e) => { console.log('token verification failed'); res.status(400).send('error') });  
  } catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
    
});

// POST /users signup
router.post('/signup', (req, res) => {
  try {
    var body = _.pick(req.body, ['email','name']);
    body.password = generatePassword(8);
    body.dp = md5(body.email);
    var user = new User(body);
    var mailOptions = {
      from: 'magguland@gmail.com',
      to: body.email,
      subject: 'Welcome to Magguland!',
      text: 'Hi ' + body.name + ', Welcome to Magguland. Your password is : ' + body.password + '.'
    };
    user.save().then(() => {
      res.status(200).json({msg:'success'})
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log('error sending mail transporter');
        }
      });
    }).catch((e) => {
        res.status(400).send(e);
    }) 
  } catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
});
  
router.post('/userinfo', authenticate, (req, res) => {
  try{
    if(req.user == null)
      res.status('401').send('unauthorized');
    else{
      User.findOne({_id:req.user.id}, {'email':1, 'name': 1, 'profilePicture':1}, {lean: true}, (err, doc) => {
        if(err || doc == null)
          res.status(400).send(err);
        else
          res.json(doc);
        
      })
    }
  } catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
});
  
router.post('/login', (req, res) => {
  try {
    var body = _.pick(req.body, ['email', 'password']);

    User.findByCredentials(body.email, body.password).then((user) => {
      return user.generateAuthToken().then((token) => {
        res.cookie('li',1, { maxAge: 30758400000, httpOnly: true, path:'/' });
        res.header('x-auth', token).send(user);
      });
    }).catch((e) => {
      res.status(400).send();
    });
  } catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
  });

router.post('/logout', authenticate, (req, res) => {
  try {
    if(req.user == null)
      res.status(400).send();
    else{
      req.user.removeToken(req.token).then(() => {
        res.status(200).send();
      }, () => {
        res.status(400).send();
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
});

router.post('/updateBasic', authenticate, async (req, res) =>{
  try {
    body = JSON.parse(req.body.data)
    if(req.user){
      User.findByIdAndUpdate(req.user.id, body, (err, docs)=>{
        if(err)
          res.status(500).send('error processing request');
        else
          res.status(200).send('Updated succesfully');
      })
    }else{
      res.status(401).send('Authourisation failed!');
    }
  } catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
})

router.post('/updateProfilePicture', authenticate, async (req, res) =>{
  try {
    body = JSON.parse(req.body.data)
    if(req.user){
      User.findByIdAndUpdate(req.user.id, body, (err, docs)=>{
        if(err)
          res.status(500).send('error processing request');
        else
          res.status(200).send(body.profilePicture);
      })
    }else{
      res.status(401).send('Authourisation failed!');
    }
  } catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
})

router.post('/getBasic',authenticate, async (req, res)=>{
  try {
  body = JSON.parse(req.body.data)
  res.json({
    userId : req.user?req.user.id:'',
    description : await userDetail(body.id,'description'),
    college : await userDetail(body.id, 'college'),
    department : await userDetail(body.id, 'department'),
    profilePicture : await userDetail(body.id, 'profilePicture'),
    googlePhoto : await userDetail(body.id, 'googlePhoto'),
    email : await userDetail(body.id, 'email')
  });
} catch (error) {
  console.log(error);
  res.status(500).send('Error proccesing request.');
  var err = error;
  if(err){
    var apiName = req.method + ' ' + req.originalUrl;
    if(typeof err == 'object'){
        var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
    }else{
        var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
    }
    notifySlack(slackText, 'prod-alerts');
  }
}
})

router.post('/getDocs', (req, res) => {
  try {
  body = JSON.parse(req.body.data);
  let hourAgoTime = new Date().getTime() - 60*60*1000
  File.find({author: body.id})
     .skip(body.skip)
     .exec(async function(err, doc) {
      if(err){
        console.log(err);
        res.json([]);
      }
      else{
        var files = [];
        for (var i = 0; i < doc.length; i++) {
            file = {
                name : doc[i].name,
                fileId : doc[i].uniqueId,
                views : parseInt((await View.find({pageType : 'read-main', pageId : doc[i].uniqueId, time : {$lt : hourAgoTime}}).exec()).length),
                time : doc[i].uploadTime,
                author : doc[i].author,
                authorName : await userDetail(doc[i].author, 'name'),
                pathData : (await Course.findOne({_id : doc[i].courseIds[0]}).exec()),
                semester : doc[i].semester,
                department : doc[i].department,
                college : doc[i].college,
                description : doc[i].description,
            }
            files.push(file);
        }
        res.json(files);
        }
     });
    } catch (error) {
      console.log(error);
      res.status(500).send('Error proccesing request.');
      var err = error;
      if(err){
        var apiName = req.method + ' ' + req.originalUrl;
        if(typeof err == 'object'){
            var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
        }else{
            var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
        }
        notifySlack(slackText, 'prod-alerts');
      }
    }
})

router.get('/:userId', authenticate , async (req, res) => {
  try {
    var li = req.cookies.li
    if(req.user)
      userId = req.user.id;
    else
      userId = null;
    try{
      res.render('profile', {
        li : li,
        userId : userId,
        picture : await userDetail(req.params.userId, 'profilePicture'),
        views : await userDetail(req.params.userId, 'views'),
        name : await userDetail(req.params.userId, 'name'),
        files : await userDetail(req.params.userId, 'files')
      });
    } catch(err){
      res.render('error');
    }
  } catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
});

async function userDetail(id,detail){

  if(detail == 'views'){
    return new Promise((resolve, reject) => {
      File.find({author:id}, async (err,doc)=>{
        if(err || doc==null)
          resolve(0);
        count = 0;
        for(i=0;i<doc.length;i++){
          count += await View.countDocuments({pageType : 'read-main', pageId : doc[i].uniqueId});
        }
        resolve(count)
      }) 
    })
  }

  if(detail == 'files'){
    return new Promise((resolve, reject) => {
      File.find({author:id}, (err,doc)=>{
        if(err || doc==null)
          resolve(0);
        resolve(doc.length)
      }) 
    })
  }

  else {
    return new Promise((resolve, reject) => {
      User.findById(ObjectId(id), function (err, user) {
          if(err) {
              reject();
              return null;
          }
          if(user)
            resolve(user[detail]);
          else
            reject()
      })
    })
  }
}