var express = require('express');
const _ = require('lodash');

var {authenticate} = require('../middleware/authenticate');
//var {File} = require('./../models/file');
var {College} = require('../models/collegeModel');

var notifySlack = require('../lib/slackNotifier');

const fileName = __filename;

var router = express.Router();
module.exports = router;

router.get('/', async (req, res) => { 
    try{
    let colleges = await College.find().exec();
    let collegeData = [];
    for(let i=0; i<colleges.length; i++){
        collegeData.push({
            name : colleges[i].name,
            shortName : colleges[i].shortName
        })
    }
    res.render('goldmine',{
        li: req.cookies.li,
        collegeData : collegeData
    });
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
});