var express = require('express');
const _ = require('lodash');

const adminList = require('./../json/adminList.json');

var {authenticate} = require('./../middleware/authenticate');
var Collections = {
    College : require('./../models/collegeModel').College,
    User : require('./../models/userModel').User,
    Course : require('./../models/courseModel').Course,
    File : require('./../models/fileModel').File,
    Experience : require('./../models/experienceModel').Experience,
    Visit : require('./../models/visitModel').Visit
}

var notifySlack = require('../lib/slackNotifier');

var router = express.Router();
module.exports = router;

router.get('/', authenticate, (req, res) => {
    try {
        if(req.user && adminList.includes(req.user.email) ){
            res.render('adminDashboard', {
                title : 'Admin'
            });
        }else{
            console.log('Access denied')
            res.render('error');
        }
    } catch (error) {
        console.log(error);
        res.send('error');
        var err = error;
        if(err){
            var fileName = 'adminApi.js';
            var apiName = 'GET /'
            if(typeof err == 'object'){
                var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
            }else{
                var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
            }
        notifySlack(slackText, 'prod-alerts');
        }
    }
})

router.post('/get/:collection', authenticate, async (req,res)=>{
    try {
        if(req.user != null && adminList.includes( req.user.email )){
            Collections[req.params.collection].find((err, docs)=>{
                
                if(err)
                    throw new Error(err);
                else{
                    var parser = collectionDataParser[req.params.collection];
                    var jsonData = parser(docs);
                    res.json(jsonData);
                }
                
            })
        }else{
            res.status(401).send('Unauthourized Request');
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error proccesing request.');
        var err = error;
        if(err){
            var fileName = 'adminApi.js';
            var apiName = 'POST ' + req.originalUrl;
            if(typeof err == 'object'){
                var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
            }else{
                var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
            }
        notifySlack(slackText, 'prod-alerts');
        }
    }
    
})

router.post('/edit/:collection', authenticate, async (req, res) => {
    try{
        if(req.user != null && adminList.includes( req.user.email )){
            var body = req.body;
            Collections[req.params.collection].findByIdAndUpdate(body._id, {$set:body}, (err, docs)=>{
                if(err)
                  res.status(500).send('error processing request');
                else
                  res.status(200).send('Updated succesfully');
              })
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error processing request');
        var err = error;
        if(err){
            var fileName = 'adminApi.js';
            var apiName = 'POST ' + req.originalUrl;
            if(typeof err == 'object'){
                var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
            }else{
                var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
            }
        notifySlack(slackText, 'prod-alerts');
        }
    }
})

router.post('/delete/:collection', authenticate, async (req, res) => {
    try{
        if(req.user != null && adminList.includes( req.user.email )){
            var body = req.body;
            Collections[req.params.collection].findByIdAndDelete(body._id, (err, docs)=>{
                if(err)
                  res.status(500).send('error processing request');
                else
                  res.status(200).send('Updated succesfully');
              })
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error processing request');
        var err = error;
        if(err){
            var fileName = 'adminApi.js';
            var apiName = 'POST ' + req.originalUrl;
            if(typeof err == 'object'){
                var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
            }else{
                var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
            }
        notifySlack(slackText, 'prod-alerts');
        }
    }
})

var collectionDataParser = {
    User : (data) => {
        var jsonData = [];
        data.forEach(user => {
            jsonData.push({
                _id : user._id,
                email : user.email,
                name : user.name,
                profilePicture : user.profilePicture,
                googlePhoto : user.googlePhoto
            });
        });
        return jsonData;
    },

    College : (data) => {
        var jsonData = [];
        data.forEach(college => {
            jsonData.push({
                _id : college._id,
                creator : college.creator,
                name : college.name,
                shortName : college.shortName,
                status : college.status,
                address : college.address,
                description : college.description,
                departments : college.departments,
                picture : college.picture,
                createdOn : college.createdOn
            })
        });
        return jsonData;
    },

    Course : (data) => {
        var jsonData = [];
        data.forEach(course => {
            jsonData.push({
                _id : course._id,
                creator: course.creator,
                name : course.name,
                shortName : course.shortName,
                department : course.department,
                college : course.college,
                paths : course.paths,
                createdOn : course.createdOn
            })
        })
        return jsonData;
    },

    File : (data) => {
        var jsonData = [];
        data.forEach(file => {
            jsonData.push({
                _id : file._id,
                name : file.name,
                url : file.url,
                category : file.category,
                author : file.author,
                college : file.college,
                courses : file.courses,
                courseIds : file.courseIds,
                type : file.type,
                uniqueId : file.uniqueId,
                description : file.description
            })
        })
        return jsonData;
    },

    Experience : (data) => {
        var jsonData = [];
        data.forEach(file => {
            jsonData.push({
                _id : file._id,
                title : file.title,
                category : file.category,
                author : file.author,
                authorName : file.authorName,
                college : file.college,
                company : file.company

            })
        })
        return jsonData;
    },
    Visit : (data) => {
        return data
    }
}