const {google} = require('googleapis');

const key = require('../ga-auth.json')
const scopes = 'https://www.googleapis.com/auth/analytics.readonly'
const jwt = new google.auth.JWT(key.client_email, null, key.private_key, scopes)
const view_id = '192991209'

process.env.GOOGLE_APPLICATION_CREDENTIALS = './auth.json'

jwt.authorize((err, response) => {
  google.analytics('v4').data.ga.get(
    {
      "reportRequests": [
       {
        "viewId": view_id,
        "dimensions": [
         {
          "name": "ga:pagePath"
         }
        ],
        "metrics": [
         {
          "expression": "ga:pageviews"
         }
        ],
        "dimensionFilterClauses": [
         {
          "filters": [
           {
            "operator": "EXACT",
            "dimensionName": "ga:pagePath",
            "expressions": [
             "/goldmine"
            ]
           }
          ]
         }
        ],
        "dateRanges": [
         {
          "startDate": "30daysAgo",
          "endDate": "today"
         }
        ]
       }
      ]
     },
    (err, result) => {
      console.log(JSON.stringify(result,undefined,2))
    }
  )
})