var express = require('express');
const router = express.Router();
const rp = require('request-promise');

router.get('/', (req, res)=>{
    res.render('covid', {
        title : 'Covid-19 Live Stats for INDIA',
        imgUrl : 'https://www.collegeeks.com/images/ogs/covid-og.jpg',
        description: 'A quick tool to remain updated about spread of Coronavirus in India. Stay tuned for more updates.'
    });
})

let data = null;

router.get('/stateData', async (req, res)=>{
    try {
        const currentTime = new Date().getTime()
        res.set('Cache-Control', 'no-store');
        
        if(data){
            res.json(data)
            data = {
                stateData : await rp('https://api.covid19india.org/data.json'),
                lastFetchedOn : new Date().getTime()
            }
        }else{
            data = {
                stateData : await rp('https://api.covid19india.org/data.json'),
                lastFetchedOn : new Date().getTime()
            }
            res.json(data);
        }
        // console.log(JSON.parse(data.stateData).statewise[0])
        // console.log(JSON.parse(await rp('https://api.covid19india.org/data.json')).statewise[0])
    } catch (error) {
        var fileName = 'covidApi.js';
        var apiName = 'POST ' + req.originalUrl;
        if(typeof err == 'object'){
            var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
        }else{
            var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
        }
        notifySlack(slackText, 'prod-alerts');
    }
})

module.exports = router