var express = require('express');
var router = express.Router();
module.exports = router;

var {authenticate} = require('./../middleware/authenticate');

router.get('/', authenticate, (req, res)=>{
    var options = {  root: __dirname + '/../views/' };
    if(req.user)
        fileName = 'header-li.html'
    else
        fileName = 'header-lo.html'
    res.sendFile(fileName, options, function (err)  {
        if(err){
            console.log(err)
        }
    });
});
