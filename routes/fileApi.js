var express = require('express');
const _ = require('lodash');
var uniqid = require('uniqid');
var request = require('request');
var ObjectId = require('mongodb').ObjectID;
var score = require('string-score');
const fs = require('fs');
const download = require('download');
const readChunk = require('read-chunk');
const fileType = require('file-type');
const {Storage} = require('@google-cloud/storage');
var formidable = require('formidable');
var path = require('path');

var {authenticate} = require('./../middleware/authenticate');
var {File} = require('./../models/fileModel');
var {College} = require('./../models/collegeModel');
var {User} = require('./../models/userModel');
var {View} = require('./../models/viewModel');
var {Course} = require('./../models/courseModel');
var dummyUsers = require('./../json/dummyUsers');

var notifySlack = require('../lib/slackNotifier');

const fileName = __filename;

var router = express.Router();
module.exports = router;

const storage = new Storage({
    projectId: 'dogwood-cinema-234903',
    keyFilename: 'keyfile.json'
  });
  
  var BUCKET_NAME = 'mlbu'
  var myBucket = storage.bucket(BUCKET_NAME)

router.post('/addfile', authenticate, (req, res) => {
    try {
    if(req.user==null)
        res.status(401).send('authentication failed');
    else{
        var body = JSON.parse(req.body.data);

        var fileObj = {
            name : body.name,
            url : body.url,
            fileSize : body.fileSize,
            description : body.description,
            category : body.category,
            uploadTime : new Date().getTime(),
            author : req.userIsAdmin ? dummyUsers[ parseInt(Math.random()*(dummyUsers.length - 1)) ]["_id"] : req.user.id ,
            college : body.college,
            courses : [body.courseName],
            courseIds : [body.courseId],
            type : body.type,
            uniqueId : uniqid()
        }

        var file = new File(fileObj);

        file.save((err, file) => {
            if(err){
                res.status(400).send(err)
            }else{
                res.status(200).send('success');
                let alertText = 'New document posted.\nhttps://collegeeks.com/read?v=' + fileObj.uniqueId + '\n*Name : ' + fileObj.name + '*\n*Course : ' + body.courseName + '*\n*Type : ' + body.type + '*\n*Category : ' + body.category + '*\n*College : ' + body.college + '*' 
                notifySlack(alertText, 'doc-alerts');
            }
        })

        if(fileObj.type == 'link' && fileObj.url.substr(fileObj.url.length - 4) == '.pdf' ){
            download(fileObj.url, './uploads').then(()=>{
                let fileName = fileObj.url.split('/')[fileObj.url.split('/').length - 1];
                let newFileName = uniqid() + decodeURI(fileName).toLowerCase().split(' ').join('-').split('#').join('');
                fs.rename( path.join(__dirname + '/../uploads/') + fileName , path.join(__dirname + '/../uploads/') + newFileName, (err)=>{
                    if(err){
                        console.log('error in renaming file!!');
                        notifySlack('Error in renaming file !! from ' + fileName + ' to ' + newFileName, 'critical-alerts');
                        fs.unlink(path.join(__dirname + '/../uploads/') + fileName, (err) =>{
                            if (err) {
                                console.log(err);
                                notifySlack('File delete failed ' + new Date().toString(), 'critical-alerts');
                              }
                        })
                    }else{
                        var buffer = readChunk.sync(__dirname + '/../uploads/' + newFileName , 0, fileType.minimumBytes)
                        var mime = fileType(buffer).mime;
                        if(mime == 'application/pdf'){
                            myBucket.upload(path.join(__dirname + '/../uploads/') + newFileName, {public : true, resumable : false}).then((file)=>{
                                let newUrl = 'https://storage.googleapis.com/mlbu/' + newFileName;
                                File.findOneAndUpdate({url : fileObj.url}, {url : newUrl, type : 'file'}, (err)=>{
                                    if(err){
                                        console.log('Error updating doc type after upload to bucket. Newurl - ' + newUrl);
                                        console.log(err);
                                        notifySlack('Error updating url after upload to bucket - url : ' + newUrl , 'critical-alert');
                                    }else{
                                        //console.log('Type updated');
                                    }
                                })
                                fs.unlink(path.join(__dirname + '/../uploads/') + newFileName, (err) =>{
                                    if (err) {
                                        console.log(err);
                                        notifySlack('File delete failed ' + new Date().toString(), 'critical-alerts');
                                      }
                                })
                            }).catch((e)=>{
                                console.log(e);
                                notifySlack('File UPLOAD FAILED!! for ' + newFileName, 'critical-alerts');
                                fs.unlink(path.join(__dirname + '/../uploads/') + newFileName, (err) =>{
                                    if (err) {
                                        console.log(err);
                                        notifySlack('File delete failed ' + new Date().toString(), 'critical-alerts');
                                      }
                                })
                            })
                        }
                    }
                })

                
            })
        }

    }
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
});

router.post('/filelist', (req, res) => {
    try {
    body = JSON.parse(req.body.data);
    File.find({courseIds : body.courseId}, async function (err, doc) {
        if(err || doc==null)
            res.status(400).send(err);
        fileList = [];
        let hourAgoTime = new Date().getTime() - 60*60*1000
        for(i=0; i<doc.length; i++){
            file = {
                name : doc[i].name,
                id : doc[i].uniqueId,
                fileType : doc[i].type,
                views : parseInt((await View.find({pageType : 'read-main', pageId : doc[i].uniqueId, time : {$lt : hourAgoTime}}).exec()).length),
                uploadTime : doc[i].uploadTime,
                author : doc[i].author,
                authorName : await username(doc[i].author),
                category : doc[i].category
            }
            fileList.push(file);
        }
        res.json(fileList)
    });
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
});

router.post('/search', (req, res) => {
    try {
    body = JSON.parse(req.body.data);
    File.find({$text: {$search: body.query}},
        {score: {$meta: "textScore"}})
       .skip(0)
       .limit(20)
       .sort( { score: { $meta: "textScore" } } )   
       .exec(async function(err, doc) {
        var files = [];
        for (var i = 0; i < doc.length; i++) {
            if(doc[i].name == body.query && body.source == 'read-main')
                continue;
            file = {
                name : doc[i].name,
                fileId : doc[i].uniqueId,
                views : await View.countDocuments({pageType : 'read-main', pageId : doc[i].uniqueId}),
                time : doc[i].uploadTime,
                author : doc[i].author,
                authorName : await username(doc[i].author),
                pathData : await Course.findOne({_id : doc[i].courseIds[0]}).exec(),
                semester : doc[i].semester,
                department : doc[i].department,
                college : doc[i].college,
                description : doc[i].description
            }
            files.push(file);
        }
        let moreDoc = await File.find({name : new RegExp(body.query, "i")}, null, {limit : 15 - doc.length}).exec()
        doc = moreDoc
        for(i=0; i<moreDoc.length; i++){
            if(moreDoc[i].name == body.query  && body.source == 'read-main')
                continue;
            file = {
                name : doc[i].name,
                fileId : doc[i].uniqueId,
                views : await View.countDocuments({pageType : 'read-main', pageId : doc[i].uniqueId}),
                time : doc[i].uploadTime,
                author : doc[i].author,
                authorName : await username(doc[i].author),
                pathData : await Course.findOne({_id : doc[i].courseIds[0]}).exec(),
                semester : doc[i].semester,
                department : doc[i].department,
                college : doc[i].college,
                description : doc[i].description
            }
            if(!files.find(f => f.fileId == file.fileId))
                files.push(file);
        }
        
        if(files.length == 0 && false){
            notifySlack('No result found for ' + body.query, 'search-alerts');
        }
        res.json(files);
       });
    } catch (error) {
        console.log(error);
        res.status(500).send('Error proccesing request.');
        var err = error;
        if(err){
          var apiName = req.method + ' ' + req.originalUrl;
          if(typeof err == 'object'){
              var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
          }else{
              var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
          }
          notifySlack(slackText, 'prod-alerts');
        }
      }
})

// router.post('/recommended', )
/**
 * POST /recommended
 * things i have user watched
 */

function username (id){
    return new Promise((resolve, reject) => {
        User.findById(ObjectId(id), function (err, user) {
            if(err||user==null) {
                resolve(null);
            }
            else resolve(user.name);
        })
    })
}
