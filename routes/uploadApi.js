var express = require('express');
var router = express.Router();
const {Storage} = require('@google-cloud/storage');
var formidable = require('formidable');
var path = require('path');
var toPdf = require('office-to-pdf');
var fs = require('fs');
var uniqid = require('uniqid');
const readChunk = require('read-chunk');
const fileType = require('file-type');
var cmd=require('node-cmd');

var notifySlack = require('../lib/slackNotifier');

const fileName = __filename;

module.exports = router;

const storage = new Storage({
  projectId: 'dogwood-cinema-234903',
  keyFilename: 'keyfile.json'
});

var BUCKET_NAME = 'mlbu'
var myBucket = storage.bucket(BUCKET_NAME)
let localFileLocation = './uploads';
var filesPath = path.join(__dirname, '../uploads/');

router.post('/', async function(req, res){
    var form = new formidable.IncomingForm();
    form.multiples = true;
    var fileObj;
    
    form.uploadDir = path.join(__dirname, '../uploads');
    
    form.on('file', function(name, file) {
        fileObj = file;
    });

    form.on('error', function(err) {
        console.log('An error has occured in file upload : \n' + err);
        notifySlack('An error occured in upload.js. Inside form.onerror. Err - ' + err, 'prod-alerts');
    });

    form.on('end', async function(){
        /**
         * Sequence ==>
         * Check mime
         * convert if not pdf
         * check again, if not pdf delete return
         * compress
         * check if compressed, if not use orignal
         * count num of pages
         * if size > 3mb split in minOf( totalSize/3mb , totalNumOfPages/15 ) return renamed array of fileNames
         * upload all
         * delete all
         */
        var filePath = fileObj.path.split('/upload_')[0] + '/';
        filePath = await getNewPath(fileObj, filePath); 
        var mime = await getMime(filePath);
        
        if(!mime.includes('pdf')){
            try {
                filePath = await convertToPdf(filePath);
            } catch (error) {
                res.status(400).send('Error converting file to pdf. ' + error);
                notifySlack('Error converting file - ' + fileObj.name + ' to pdf. Error - ' + error, 'critical-alerts');
                //deleteFile(fileObj);
                return;
            }
        }

        try {
            var minFilePath = await compressPdf(filePath);
        } catch (error) {
            var minFilePath = filePath;
            notifySlack('Error compressing file ' + fileObj.name + ' ' + error, 'critical-alerts');
        }

        if( fs.statSync(minFilePath).size > fs.statSync(filePath).size){
            minFilePath = filePath;
        }
        
        // var fileObjs = splitFile(minFileObj);
        // var fileNames = [];

        // for(i=0; i<fileObjs.length; i++){
        //     fileNames.push( 'https://storage.googleapis.com/mlbu/' + fileObjs[i].name);
        // }
        
        // res.json(fileNames);

        // for(let i=0; i<fileObjs.length; i++){
        //     uploadFile(fileObjs[i]);
        // }

    }); 
    form.parse(req);
});

async function getNewPath(fileObj, filePath){
    return new Promise((resolve, reject)=>{
        var newPath = filePath + uniqid() + '-' + fileObj.name.toLowerCase().split(' ').join('-').split('#').join('');
        fs.rename(fileObj.path, newPath ,(err)=>{
            if(err){
                notifySlack('Error renaming file ' + err, 'critical-alerts');
                console.log(err);
                reject(err);
            }
            resolve(newPath);
        } )
    })
}

async function getMime(filePath){
    return new Promise((resolve, reject)=>{
        var buffer = readChunk.sync(filePath, 0, fileType.minimumBytes);
        resolve( fileType(buffer).mime );
    })
}

async function convertToPdf(filePath){
    return new Promise((resolve, reject)=>{
        officeBuffer = fs.readFileSync(filePath);

        toPdf(officeBuffer).then((pdfBuffer)=>{
            fs.writeFileSync( filePath + '.pdf', pdfBuffer);
            resolve(filePath + '.pdf') ;
        }, (err)=>{
            notifySlack('Error converting to pdf - ' + fileObj.name + error, 'critical-alerts');
            reject(error);
        });
    })    
}

async function compressPdf(filePath){
    return new Promise((resolve, reject)=>{
        cmd.get(
            'gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=' + filePath + '.min.pdf' + ' ' + filePath,
            function(err, data, stderr){
                if(err){
                    console.log(err);
                    notifySlack("Error compressing pdf " + fileObj.name + err, 'critical-alerts');
                    reject(err);
                }else{
                    resolve(filePath + '.min.pdf');
                }
            }
        );
    })
}

async function splitFile(fileObj){
    var fileSize = fileObj.size;
    try {
        var filePages = await countNoOfPages(fileObj);
    } catch (error) {
        console.log(error);
        var filePages = null;
    }
    var fileObjs = [];
    if(fileSize && fileSize > 1024*1024*2 ) // > 2mb
    {
        if(filePages){
            sizeSplit = parseInt(fileObj.size/(1024*1024*3));
            pageSplit = filePages/15;
            if(pageSplit > sizeSplit)
                pageSplit = sizeSplit;
            for(let i=0; i<pageSplit; i++){
                fileObjs.push( await splitPdf(fileObj, i, pageSplit) )
            }
            return fileObjs;
        }else{
            return [fileObj];
        }
    }else{
        return [fileObj];
    }
}

async function countNoOfPages(fileObj){
    cmd.get(
        'pdftk pdffile.pdf dump_data|grep NumberOfPages| awk "{print $2}"',
        function(err, data, stderr){
            if(err){
                console.log(err);
                notifySlack("Error counting pages in pdf " + fileObj.name + err, 'critical-alerts');
                return null;
            }else{
                return data;
            }
        }
    )
}

async function splitPdf(fileObj, split, totalSplit){
    cmd.get(
        'pdftk ' + fileObj.name + ' cat 1-2 4-5 output mynewfile.pdf'
    )
}