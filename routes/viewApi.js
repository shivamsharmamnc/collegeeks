var express = require('express');
var ObjectId = require('mongodb').ObjectID;

var {authenticate} = require('../middleware/authenticate');
var {File} =  require('./../models/fileModel');
var {User} =  require('./../models/userModel');
var {View} =  require('./../models/viewModel');

var notifySlack = require('../lib/slackNotifier');

const fileName = __filename;

var router = express.Router();
module.exports = router;

router.post('/', authenticate, (req, res) => {
    try {
    currentTime = new Date().getTime();
    fiveSecBefore = currentTime - 5000;
    requestIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    // RULE : Dont count view for a user within 10 second
    let apiUser = req.user ? req.user.id : 12345;
    obj = {
        time : new Date().getTime(),
        pageId : req.body.pageId,
        pageType : req.body.pageType,
        college : req.body.college,
        user : req.user?req.user.id:null,
        ip : req.headers['x-forwarded-for'] || req.connection.remoteAddress
    }
    var view = new View(obj);
    view.save((err) => {
        if(err){
            console.log(err)
        }
    })
    // View.find({ $or : [{ip : requestIp , user : null, time : {$gt : fiveSecBefore}}, {user : apiUser , time : {$gt : fiveSecBefore}}]}, (err, docs)=>{
    //     if(err){
    //         console.log(err);
    //     }else{
    //         if(docs && false){
    //             console.log('Request by ' + apiUser + ' is within 5 sec');
    //         }else{
                
    //         }
    //     }
    // })

    res.send('success');
} catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
})
