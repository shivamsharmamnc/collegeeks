var express = require('express');
const _ = require('lodash');
var uniqid = require('uniqid');
var request = require('request');
var ObjectId = require('mongodb').ObjectID;
var score = require('string-score');
const md5 = require('md5');

var {authenticate} = require('./../middleware/authenticate');
var {File} = require('./../models/fileModel');
var {College} = require('./../models/collegeModel');
var {Course} = require('./../models/courseModel');
var {User} = require('./../models/userModel');
var {Experience} = require('./../models/experienceModel');
var {Visit} = require('./../models/visitModel');

var dummyUsers = require('../json/dummyUsers.json');

var router = express.Router();
module.exports = router;

router.post('/perform', authenticate , async (req, res) => {
    try {
        if(req.userIsAdmin){
            let taskDone = await randomizePoster();
            if(taskDone)
                res.send('Task Completed')
            else
                res.status(500).send('Error executing task. Check logs.');
        }else{
            console.log('User not admin')
            res.status(400).send('Admin access required');
        }
    } catch (error) {
        res.status(500).send('Error executing task! ' + error);
        console.log(error);
    }    
})

async function randomizePoster(){
    let docs = await Experience.find({ $or : [{author : '5d6e0d65b386075565ac384b'}, {author : '5d63d189391566707dfa48e0'}, {author : '5ca5a95fe2a7c16be232b3a6'}] }).exec();
    for(let i=0; i<docs.length; i++){
        let doc = docs[i];
        let newAuthor = getRandomAuthor();

        await Experience.findOneAndUpdate({_id : doc._id}, {author : newAuthor._id , authorName : newAuthor.name}).exec()

    }
    return true;
}

async function randomizeUploader(){
    let docs = await File.find({ $or : [{author : '5d6e0d65b386075565ac384b'}, {author : '5d63d189391566707dfa48e0'}, {author : '5ca5a95fe2a7c16be232b3a6'}] }).exec();
    for(let i=0; i<docs.length; i++){
        let doc = docs[i];
        let docCourse = await Course.findOne({_id : ObjectId(doc.courseIds[0])}).exec();
        if(docCourse){
            let department = docCourse.department;
            let newAuthor = await getRandomAuthorByDep(department);
            await File.findOneAndUpdate({_id : doc._id}, {author : newAuthor}).exec()
        }else{
            console.log(doc._id);
        }
    }
    return true;
}

async function myTask(){
    for(i=2; i<dummyUsers.length; i++){
        let dUsr = dummyUsers[i];
        dUsr = dUsr.split(',');
        let eHash = md5(dUsr[3]);
        let pic = 'https://www.gravatar.com/avatar/'+eHash+'?s=100&d=robohash'
        let userObj = {
            name : dUsr[1],
            email : dUsr[3],
            googlePhoto : pic,
            profilePicture : pic,
            password : 'abcd1234',
            timeLag : 1000,
            filesUploaded : [],
            filesDownloaded : [],
            filesViewed : [],
            bookshelf : [],
            tokens : []
        }
        let user =  new User(userObj);
        let saveUser = await user.save();
        console.log(saveUser); 
    }
    
    return true;
}

function getRandomAuthor(){
    let totalRandomAuthors = dummyUsers.length;
    
    return dummyUsers[ Math.floor(totalRandomAuthors*Math.random()) ];
}

async function getRandomAuthorByDep(department){
    try {
        let randomAuthors = [];
        for(let i=0; i<dummyUsers.length; i++){
            if(dummyUsers[i].dep == department){
                randomAuthors.push(dummyUsers[i])
            }
        }
        let totalRandomAutors = randomAuthors.length;
        let randomIndex = parseInt(totalRandomAutors*Math.random());
        return randomAuthors[randomIndex]._id;
    } catch (error) {
        console.log(error);
    }
}