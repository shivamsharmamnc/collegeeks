const router = require('express').Router();

const Comment = require('../models/commentModel');
const {authenticate} = require('../middleware/authenticate');

router.get('/getThreadComments/:threadId', async (req, res)=>{
    try {
        let comments = await Comment.find({threadId : req.params.threadId}).exec();
        res.json(comments);
    } catch (error) {
        console.log(error);
    }
});

router.post('/create', authenticate,  async (req, res)=>{
    try {
        if(req.user){
            let commentObj = req.body;
            commentObj.timestamp = new Date().getTime();
            commentObj.author = req.user._id;
            commentObj.authorName = req.user.name;

            let comment = new Comment(commentObj);
            await comment.save((err)=>{
                if(err){
                    console.log(err);
                    res.status(500).send(err);
                }
                res.send('OK');
            });
        }else{
            res.status(400).send('Please sign in to create comment');
        }
    } catch (error) {
        
    }
})

module.exports = router;