var express = require('express');
var ObjectId = require('mongodb').ObjectID;

var {authenticate} = require('../middleware/authenticate');
var {User} =  require('./../models/userModel');
var {View} =  require('./../models/viewModel');
var {Vote} = require('./../models/voteModel');
const {News} = require('./../models/newsFeedModel');
const Comment = require('../models/commentModel');

var notifySlack = require('../lib/slackNotifier');

const fileName = __filename;

var router = express.Router();
module.exports = router;

router.post('/save', authenticate,  async (req, res)=>{
  try {
    if(req.user){
      let newsObj = req.body;
      newsObj.timestamp = new Date().getTime();
      newsObj.author = req.user._id;
      newsObj.authorName = req.user.name;

      let news = new News(newsObj);

      await news.save();
      res.send('OK');
      notifySlack('New news posted', 'college-alerts');
    }else{
      res.status(400).send('Please login to create post.!')
    }

  } catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
});

router.get('/imgLink', (req, res)=>{
  res.send('OK');
  notifySlack('Add link ' + req.query.link, 'college-alerts');
})

router.get('/loadNews', async (req, res)=>{
  try {
    let news = await News.find({college : req.query.college}).lean().exec();
    news = news.reverse();
    for(let i=0; i<news.length; i++){
      news[i].authorPic = (await User.find({_id : news[i].author}))[0].profilePicture;
      news[i].votes = (await Vote.find({itemId : 'news-' + news[i]._id})).length;
      news[i].comments = (await Comment.find({threadId : news[i]._id})).length;
    }
    res.json(news);
  } catch (error) {
    console.log(error);
    res.status(500).send('Error proccesing request.');
    var err = error;
    if(err){
      var apiName = req.method + ' ' + req.originalUrl;
      if(typeof err == 'object'){
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + JSON.stringify(err, Object.getOwnPropertyNames(err), 2) + '```';
      }else{
          var slackText = '*File : ' + fileName + '*\n*API : ' + apiName + '*\nError - ```' + err + '```';
      }
      notifySlack(slackText, 'prod-alerts');
    }
  }
})