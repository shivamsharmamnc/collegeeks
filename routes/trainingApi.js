var express = require('express');
const _ = require('lodash');
const request = require('request-promise');
const urlMetadata = require('url-metadata')

var {authenticate} = require('./../middleware/authenticate');
var {User} = require('./../models/userModel');
var {Visit} = require('./../models/visitModel');
var {View} = require('./../models/viewModel');
var {Experience} = require('./../models/experienceModel');
var moreColleges = require('./../json/collegeList2');
var tyre1CollegeList = require('./../json/tyre1CollegeList');
var moreCompanies = require('./../json/companyList');
var dummyUsers = require('../json/dummyUsers.json');

var notifySlack = require('../lib/slackNotifier');
var {sendMail} = require('../lib/mailer');
var getViewCount = require('./../lib/viewsServices');

var router = express.Router();
module.exports = router;

router.post('/placementSheet/addExperience', authenticate, (req, res)=>{
    if(req.user){
        let experienceObj = {
            title : req.body.title,
            description : getDescription(req.body.description),
            college : req.body.college,
            company : req.body.company,
            category : req.body.category,
            images : req.body.images,
            timestamp : new Date().getTime(),
            author : req.user._id,
            authorName : req.user.name,
            views : 0,
            viewsUpdatedOn : new Date().getTime(),
            shift : parseInt(Math.random()*9)
        }
        if(req.userIsAdmin){
            let randomAuthor = getRandomAuthor();
            experienceObj.author = randomAuthor._id;
            experienceObj.authorName = randomAuthor.name;
        }

        if(!experienceObj.title){
            experienceObj.title = experienceObj.company + ' @ ' + experienceObj.college
        }
        if(experienceObj.company && experienceObj.college ){
            let experience = new Experience(experienceObj);
            experience.save({},async (err)=>{
                let postedColleges = await Experience.find().distinct("college",{}).exec();
                postedColleges = postedColleges.concat(moreColleges)
                let postedCompanies = await Experience.find().distinct("company",{}).exec();
                postedCompanies = postedCompanies.concat(moreCompanies)
                
                if(!postedColleges.includes(experienceObj.college)){
                    notifySlack('New College posted in experience - ' + experienceObj.college , 'placement-sheet-alerts');
                }
                if(!postedCompanies.includes(experienceObj.company)){
                    notifySlack('New Company posted in experience - ' + experienceObj.company , 'placement-sheet-alerts');
                }
                if(err){
                    res.status(500).send('Error creating post');
                    console.log(err);
                    notifySlack('Error saving experience ```' + err + '```', 'prod-alerts');
                }else{
                    res.status(200).send('Success');
                    
                }
            })
        }else{
            res.status(400).send('Please fill all required input are required input');
        }
        
    }else{
        res.status(401).send('Please sign in to create post.');
    }
})

router.post('/placementSheet/listExperience', async (req, res)=>{
    let skip = req.headers && req.headers.skip ? req.headers.skip : 0;
    let count = 50;
    let expJson = [];
    var filter = {}
    let a;
    req.body.company != "0" ? filter.company = req.body.company : a ;
    req.body.category != "0" ? filter.category = req.body.category : a;

    if(req.body.college == 'tyre1'){
        let options = []
        for(let c=0; c<tyre1CollegeList.length; c++){
            options.push({
                college : tyre1CollegeList[c]
            })
        }
        filter["$or"] = options
    }else if(req.body.college != "0"){
        filter.college = req.body.college
    }

    let allExp = await Experience.find(filter).exec();
    allExp = allExp.reverse();
    if(skip*count + count > allExp.length)
        newCount = allExp.length - skip*count;
    else newCount = count;
    for(i=skip*count ; i<skip*count + newCount; i++){
        expJson.push(allExp[i])
    }
    res.json({data : expJson, length : allExp.length});
})

router.get('/fetchUrl', async (req, res)=>{
    try {
        urlMetadata(req.query.url).then(
        function (metadata) { // success handler
            let metaData = {
                meta : {
                    url : req.query.url,
                    title : metadata['og:title'],
                    domain : metadata['source'],
                    description : metadata['og:description'],
                    image : {
                        url : metadata['og:image']
                    }
                },
                success : 1
            }
            res.json(metaData)
        },
        function (error) { // failure handler
            console.log(error)
            res.json({success:0})
        })
    } catch (error) {
        console.log(error);
    }
})

router.get('/placement-sheet/experience/:experience', async (req, res)=>{
    let result = await Experience.findOne({_id : req.query.v})
    res.render('experienceRead', {
        pageId : req.query.v,
        imgUrl: getDocThumbnail(),
        title : result.title,
        views : await getViewCount(req.query.v , 'exp-read-main'),
        authorId : result.author,
        authorName : result.authorName,
        description : prepareDescription(result.description[0]),
        authorImage : (await (User.findOne({_id : result.author}).exec())).profilePicture,
    })
})

router.get('/placement-sheet/listVisits', async (req, res)=>{
    res.json( await Visit.find().exec() );
})

router.get('/placementSheet/getExperience/:expId', async (req, res)=>{
    res.json( (await Experience.findOne({ _id : req.params.expId })).description )
})

router.get('/placement-sheet/:batch', (req, res)=>{
    if(req.params.batch == '2019-20'){
        res.render('placementSheet', {
            title : 'Placement Sheet - ' + req.params.batch,
            imgUrl : 'https://www.collegeeks.com/images/ogs/companies-logo.jpg',
            description : 'Post and Share exam news and questions for Placement session 2019-20'
        });
    }else{
        res.render(error);
    }
})

router.post('/placementSheet/postVisit', authenticate, async (req, res)=>{
    if(req.user){
        let visitObj = {
            author : req.user._id,
            authorName : req.user.name,
            timestamp : new Date().getTime(),
            college : req.body.college,
            company : req.body.company,
            date : req.body.date
        }
        if(req.userIsAdmin){
            let randomAuthor = getRandomAuthor();
            visitObj.author = randomAuthor._id;
            visitObj.authorName = randomAuthor.name;
        }

        if(visitObj.college && visitObj.company && visitObj.date){
            let visit = new Visit(visitObj);
            visit.save(async (err)=>{
                if(err){
                    notifySlack('Error saving visit ' + '```' + err + '```' , 'placement-sheet-alerts');
                }else{
                    res.send('Success');
                    let postedColleges = await Experience.find().distinct("college",{}).exec();
                    postedColleges = postedColleges.concat(moreColleges)
                    let postedCompanies = await Experience.find().distinct("company",{}).exec();
                    postedCompanies = postedCompanies.concat(moreCompanies)
                    
                    if(!postedColleges.includes(visitObj.college)){
                        notifySlack('New College posted in visit - ' + visitObj.college , 'placement-sheet-alerts');
                    }
                    if(!postedCompanies.includes(visitObj.company)){
                        notifySlack('New Company posted in visit - ' + visitObj.company , 'placement-sheet-alerts');
                    }
                }
            })
        }else{
            res.status(400).send('Please fill all required fields.');
        }
    }else{
        res.status(500).send('Please signin to post visit.')
    }


})

router.get('/getCollegeList', async (req, res)=>{
    let postedColleges = await Experience.find().distinct("college",{}).exec();
    postedColleges = postedColleges.concat(moreColleges)
    let collegeList = uniq(postedColleges);
    res.json(collegeList);
})

router.get('/getCompanyList', async (req, res)=>{
    let postedCompanies = await Experience.find().distinct("company",{}).exec();
    postedCompanies = postedCompanies.concat(moreCompanies)
    let companyList = uniq(postedCompanies);
    res.json(companyList);
})


function getDocThumbnail(){
    return 'https://www.collegeeks.com/images/icons/deps/24-placement.png'
}

function prepareDescription(data){
    let desc = "";
    for(let i=0; i<data.blocks.length; i++){
        let block = data.blocks[i];
        if(block.type == 'linkTool'){
            desc += " " + block.data.link + " ";
        }else if(block.type == 'header'){
            desc += " " + block.data.text + " "
        }else if(block.type == 'paragraph'){
            desc += " " + block.data.text + " "
        }else if(block.type == 'list'){
            desc += " " + block.data.items.join(' ') + " "
        }else if(block.type == 'code'){
            desc += " " + block.data.code.split('\n').join(' ') + " "
        }else if(block.type == 'table'){
            desc += " " + block.data.content.join(' ') + " "
        }else if(block.type == 'image'){
            desc += " -image- "
        }
    }
    return desc.trim().split('  ').join(' ');
}
function uniq(a) {
    return a.sort().filter(function(item, pos, ary) {
        return !pos || item != ary[pos - 1];
    })
}
function getDescription(desc){
    for(let i=0; i<desc.blocks.length; i++){
        if(desc.blocks[i].type == 'image')
            desc.blocks[i].data.url = desc.blocks[i].data.file.url
            desc.blocks[i].data.stretched = true
    }
    return desc
}
function getRandomAuthor(){
    let totalRandomAuthors = dummyUsers.length;
    return dummyUsers[ Math.floor(totalRandomAuthors*Math.random()) ];
}
